import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.regex.PatternSyntaxException;


/**
 * Created by Switha on 4/11/14.
 * GUI for the archive
 * allows browsing older actions and sending them
 */
public class GUIArchive extends JFrame implements ActionListener{

    private static final long serialVersionUID = 35;

    private JTable table;
    private JPanel outer = new JPanel();

    private JPanel tableHolder = new JPanel();
    private JPanel filterHolder = new JPanel();

    private JTextField nameFilterField = new JTextField(10);
    private JTextField dateFilterField = new JTextField(10);
    private JTextField deletionField = new JTextField(4);

    private java.util.List<ArchiveObject> actions;

    private JButton send = new JButton("Send");
    private JButton back = new JButton("Back");
    private JButton delete = new JButton("Delete");
    private TableRowSorter<tableModel> sorter;
    private tableModel model;




    /**
     * constructs a new archive which can be used to browse actions and send them
     */
    public GUIArchive() {
        super("GUIArchive");
        //setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        Container con = this.getContentPane();

        con.add(outer);

        ArchiveDatabase db = new SQLDatabase();
        actions = db.readArchive();


        createTable();
        outer.setLayout(new BorderLayout());
        outer.add(filterHolder, BorderLayout.NORTH);
        filterHolder.setLayout(new FlowLayout());
        filterHolder.add(back);
        filterHolder.add(nameFilterField);
        filterHolder.add(dateFilterField);
        filterHolder.add(send);

        filterHolder.add(deletionField);
        filterHolder.add(delete);
        delete.addActionListener(this);
        send.addActionListener(this);
        outer.add(tableHolder, BorderLayout.CENTER);
        tableHolder.setLayout(new BorderLayout());
        tableHolder.add(new JScrollPane(table), BorderLayout.CENTER);
        pack();
        setVisible(true);
        back.addActionListener(this);
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);


    }

    private void createTable() {
        model = new tableModel(actions);
        table = new JTable(model);

        sorter = new TableRowSorter<tableModel>(model);
        table.setRowSorter(sorter);
        table.setFillsViewportHeight(true);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        nameFilterField.getDocument().addDocumentListener(
                new DocumentListener() {
                    @Override
                    public void insertUpdate(DocumentEvent documentEvent) {
                        nameFilter();
                    }

                    @Override
                    public void removeUpdate(DocumentEvent documentEvent) {
                        nameFilter();
                    }

                    @Override
                    public void changedUpdate(DocumentEvent documentEvent) {
                        nameFilter();
                    }
                }
        );

        dateFilterField.getDocument().addDocumentListener(
                new DocumentListener() {
                    @Override
                    public void insertUpdate(DocumentEvent documentEvent) {
                        dateFilter();
                    }

                    @Override
                    public void removeUpdate(DocumentEvent documentEvent) {
                        dateFilter();

                    }

                    @Override
                    public void changedUpdate(DocumentEvent documentEvent) {
                        dateFilter();

                    }
                }
        );


        //table.setAutoCreateRowSorter(true);


    }


    private void nameFilter(){
        RowFilter<tableModel,Object> rf=null;
        try {
            rf = RowFilter.regexFilter("(?i)"+ nameFilterField.getText(), 0);
        } catch(PatternSyntaxException e){return;}
        sorter.setRowFilter(rf);
    }

    private void dateFilter(){
        RowFilter<tableModel,Object> rf=null;
        try {
            rf = RowFilter.regexFilter("(?i)"+ dateFilterField.getText(), 1);
        } catch(PatternSyntaxException e){return;}
        sorter.setRowFilter(rf);
    }


    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        Object source = actionEvent.getSource();
        if(source==send){
            UtilsV2.showPrompt();

            try {
                ArchiveObject action =(actions.get(table.convertRowIndexToModel(table.getSelectedRow())));

                //make sure all required files exist and then try to send the report
                File file = new File(action.getActionObject().getFileName()+".pdf");
                if(!file.exists()) new CreatePDF(action.getActionObject());
                file = new File(action.getActionObject().getFileName()+".xls");
                if(!file.exists()) new CreateExcel(action.getActionObject());

                new SendReport(action.getActionObject());

            } catch (Exception e) {
                UtilsV2.disablePrompt();
                JOptionPane.showConfirmDialog(null,e.getMessage(),"",JOptionPane.OK_OPTION,JOptionPane.ERROR_MESSAGE);
                return;
            }
            UtilsV2.disablePrompt();
        }
        if(source==back){

            dispose();
        }
        if(source == delete){
            try{
            int id = Integer.parseInt(deletionField.getText());
                if(id == 0)
                    throw new Exception("Deletion of model is not allowed!");
                int j = JOptionPane.showConfirmDialog(null,"Are you sure you wish to delete ID: "+id+"? \nThe only way to reverse this is to use a backed up database.","Confirm deletion",JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE);
                if(j == 0){
                    SQLDatabase db = new SQLDatabase();
                    if(db.delete(id)){
                        this.dispose();
                        new GUIArchive();
                    }
                }

            }
            catch (Exception e){
                JOptionPane.showMessageDialog(null,e.getMessage(),"Something went wrong",JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    /*
    the is the table model that is used to create the JTable
     */
    //added private maybe this could lead to problem :s TODO
    private class tableModel extends AbstractTableModel {
        private static final long serialVersionUID = 35;

        private java.util.List<ArchiveObject> actions;

        public tableModel(java.util.List<ArchiveObject> actions) {
            this.actions = actions;
        }



        @Override
        public int getRowCount() {
            return actions.size();
        }

        @Override
        public String getColumnName(int column) {
            switch (column) {
                case 0:
                    return "Name:";
                case 1:
                    return "Date:";
                case 2:
                    return "ID:";

            }
            return null;
        }

        @Override
        public int getColumnCount() {
            return 3;
        }

        @Override
        public Object getValueAt(int i, int i2) {
            ArchiveObject action = actions.get(i);
            switch (i2) {
                case 0:
                    return action.getActionObject().getName();
                case 1:
                    return action.getActionObject().getDate();
                case 2:
                    return action.getId();
            }
            return null;
        }

    }


}