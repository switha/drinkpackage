import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Switha on 3/17/14.
 * GUI
 * The main menu of the system
 * allows selection of items, entering finalcheck and the archive
 * will allow entering settings in the future
 */
public class GUIMainMenu extends JFrame implements ActionListener, DocumentListener {

    private static final long serialVersionUID = 35;


    private int currentPage = 0;

    private final boolean debug = true;


    private ActionObject actionObject;

    private Frame archiveFrame=null;

    private JTextField author = new JTextField(6);
    private JTextField actionName = new JTextField(6);

    private final Font f = UtilsV2.getFont();



    private ArrayList<JButton> buttons=new ArrayList<JButton>();
    private JButton finish = new JButton("Finish");
    private JButton archive = new JButton("Archive");

    private JComboBox authors;
    private boolean authorsSet=false;

    private int[] size = {3,3};

    private JButton left = new JButton(" < ");
    private JButton right = new JButton(" > ");
    private JButton other = new JButton("Other");
    private JButton settings = new JButton("Settings");

    private JPanel outer = new JPanel();
    private JPanel holder = new JPanel();


    /**
     * creates the main menu of the system, allows selection of items -> GUIItemSelectWithNavigation
     * @param actionObject
     */
    GUIMainMenu(ActionObject actionObject){
        super("Drink Package");
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

        System.out.println("^^ OPENING MAIN MENU");

        addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent we)
            {
                String ObjButtons[] = {"Quit","Cancel"};
                int PromptResult = JOptionPane.showOptionDialog(null,
                        "Are you sure you want to exit?\nYou can open the action again\n when you start DrinkPackage again.",
                        "Are you sure?",
                        JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null,
                        ObjButtons,ObjButtons[1]);
                if(PromptResult==0)
                {
                    System.exit(0);
                }
            }
        });
        this.actionObject=actionObject;
        actionName.setText(actionObject.getName());

        actionName.getDocument().addDocumentListener(this);
        author.getDocument().addDocumentListener(this);
        left.setEnabled(false);

        left.addActionListener(this);
        right.addActionListener(this);

        finish.addActionListener(this);
        settings.addActionListener(this);
        archive.addActionListener(this);

        makeGooey();

        setVisible(true);
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);



    }
    private void makeGooey(){
        Container con = this.getContentPane();
        outer = new JPanel();
        outer.setLayout(new BorderLayout());
        outer.add(createButtons(),BorderLayout.CENTER);
        outer.add(createBottomPanel(),BorderLayout.SOUTH);
        con.removeAll();
        con.add(outer);
        repaint();
        revalidate();
    }

    /*
    @purpose creates an array of JButtons according to the categories
     */
    private JPanel createButtons(){

        buttons = new ArrayList<JButton>();

        if(currentPage==0) {
            for (Category category : actionObject.getCategories()) {

                String categoryName = category.getCategoryName();
                if(categoryName.equals("Other")) continue;
                JButton temp = new JButton(category.getCategoryName());
                buttons.add(temp);
                temp.setFont(f);
                temp.addActionListener(this);


            }
            buttons.add(other);
            other.addActionListener(this);
            other.setFont(f);
            finish.setFont(f);
            buttons.add(finish);
            /*
            //make sure we have only one listener
            ActionListener[] listeners = finish.getActionListeners();
            if(listeners.length==0) {
                finish.addActionListener(this);
                settings.addActionListener(this);
                archive.addActionListener(this);
            }
            */
        }
        if(currentPage==1){
           // ActionListener[] listeners = archive.getActionListeners();
            buttons.add(archive);
            archive.setFont(f);

            buttons.add(settings);
            settings.setFont(f);

            /*
            if(listeners.length==0){
                finish.addActionListener(this);
                settings.addActionListener(this);
                archive.addActionListener(this);
            }
            */

        }


        holder = new JPanel();
        holder.setLayout(new GridLayout(size[0],size[1]));

        for(JButton b : buttons)
            holder.add(b);

       return holder;

    }

   private JPanel createBottomPanel(){

       JPanel textFields=new JPanel();

       textFields.setLayout(new FlowLayout());
       getAuthors();
       if(!authorsSet) {
           author.setFont(f);
           author.setText(actionObject.getAuthor());
           textFields.add(author);
       }else{
           textFields.add(authors);
       }

       JLabel nameLabel = new JLabel("Action name: ");
       nameLabel.setFont(f);
       textFields.add(nameLabel);
       actionName.setFont(f);
       textFields.add(actionName);
       JLabel authorLabel = new JLabel("Author: ");
       authorLabel.setFont(f);
       textFields.add(authorLabel);

       textFields.add(left);
       textFields.add(right);
       left.setFont(f);
       right.setFont(f);
       return textFields;

   }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        Object source =actionEvent.getSource();
        if(source==settings){
            new GUISettings();
            return;
        }
        if(source==other){
            new GUIOther();
            dispose();
            return;
        }
        if(source==right){
            String pwd = (String) JOptionPane.showInputDialog(null,"Please enter Password","");
            if(UtilsV2.authenticate(pwd)) {
                right.setEnabled(false);
                left.setEnabled(true);
                currentPage = 1;
                makeGooey();
            }  else if(pwd!=null)JOptionPane.showMessageDialog(null,"Incorrect Password!","Incorrect",JOptionPane.WARNING_MESSAGE);

            return;
        }
        if(source==left){
            right.setEnabled(true);
            left.setEnabled(false);
            currentPage=0;
            makeGooey();
            return;
        }
        if(source==finish)  {
            new GUIFinalCheck(actionObject);
            return;
        }

        if(source==archive){
            archiveFrame=new GUIArchive();
            return;
        }

        int index=-1;

        for(int i=0;i<buttons.size();i++){
            if(source==buttons.get(i)){
                index=i;
            }
        }
        if(index>-1&&index<actionObject.getCategories().size()){
            new GUIItemSelectWithNavigation(actionObject,index);
            dispose();

        }

        /*
        if(index>-1&&index<actionObject.getCategories().size()&&(selector ==null||!selector.isShowing()))
           selector= new GUIItemSelect(actionObject.getCategories().get(index), actionObject);
       */ //category 'opener' to be implemented
    }







    @Override
    public void insertUpdate(DocumentEvent documentEvent) {
        changedUpdate(documentEvent);
    }

    @Override
    public void removeUpdate(DocumentEvent documentEvent) {
        changedUpdate(documentEvent);
    }

    @Override
    public void changedUpdate(DocumentEvent documentEvent) {
        Document source = documentEvent.getDocument();

        if(source==author.getDocument()){
            actionObject.setAuthor(author.getText());
            if(debug) System.out.println(actionObject.getAuthor());
        }
        if(source==actionName.getDocument()){
            actionObject.setName(actionName.getText());
            if(debug)System.out.println(actionObject.getName());
        }

    }

    private void getAuthors(){
        File f = new File("authors.cfg");
        if(!f.exists()){
            authorsSet=false;
            return;
        }
        List<String> a = new ArrayList<String>();
        Scanner sc = null;
        try {
             sc = new Scanner(new FileReader(f));
            while(sc.hasNextLine())
                a.add(sc.nextLine());
                authors=new JComboBox(a.toArray());
            for (String s : a) {
                if(s.equals(actionObject.getAuthor())) authors.setSelectedItem(s);
            }
        } catch (FileNotFoundException e) {
           authorsSet=false;
            return;

        } finally{
            if(sc != null)
                sc.close();
        }
        authors.setFont(this.f);
        authorsSet=true;
    }



}
