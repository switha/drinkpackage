import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Swith on 22/4/2014.
 * Generates a monthly report from the supplied list of actionObject
 */
public class MonthlyReport {


    /**
     * processes all the actions supplied in @param and creates a list of items,
     * use getList method to retrieve it
     *
     * @param inputList
     */
    public MonthlyReport(List<ActionObject>[] inputList, int month, int year) {
        List<Item>[] monthlyItems = new List[31];

        for (int i = 0; i < 31; i++) {
            monthlyItems[i] = processActions(inputList[i], false);

        }


        try {
            new CreateExcel(monthlyItems, false, true, month, year);

            new SendReport("Monthly_" + month+"_20"+year);
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "An Error has occured while sending the yearly report, please report this to your administrator!\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            UtilsV2.disablePrompt();

        }


        monthlyItems = new List[31];
        for (int i = 0; i < 31; i++) {
            monthlyItems[i] = processActions(inputList[i], true);
        }
        for (List<Item> monthlyItem : monthlyItems) {
            for (Item item : monthlyItem) {
                System.out.println(item.getName()+": "+item.getAmountSold());
            }
        }

        try {
            new CreateExcel(monthlyItems, true, true, month, year);
            new SendReport("Monthly_Other_" + month+"_20"+year);
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "An Error has occured while creating the monthly report for Other, please report this to your administrator." + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }


        UtilsV2.disablePrompt();
    }


    private List<Item> processActions(List<ActionObject> inputList, boolean other) {
        List<Item> totalSold = new ArrayList<>();
        for (ActionObject inputActionObject : inputList) {

            for (Category inputCategory : inputActionObject.getCategories()) {
                if(inputCategory.getCategoryName().equals("Other")&&!other)
                    continue;
                if(!inputCategory.getCategoryName().equals("Other")&&other)
                    continue;
                inputItemLoop:
                for (Item inputItem : inputCategory.getItems()) {
                    //iterate through totalSold
                    for (Item finalItem : totalSold) {
                        if (finalItem.equals(inputItem)) {
                            System.out.println("We added something!");
                            finalItem.addAmount(inputItem.getAmountSold());
                            continue inputItemLoop;
                        }
                    }
                    //the item doesn't exist, otherwise we wouldn't get here. Add it
                    System.out.print("Adding the item: ");
                    System.out.println(inputItem.getName());
                    totalSold.add(inputItem);

                }
            }
        }

        return totalSold;
    }



}
