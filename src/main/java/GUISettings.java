import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * Created by Swith on 7/5/2014.
 * This will be used to edit the action and category names
 * It for mouse and kbd interface
 */
public class GUISettings extends JFrame implements ActionListener {

    private static final long serialVersionUID = 35;


    public static void main(String[] args) {
        //Disable boldface controls.
        new GUISettings();

        //Display the window.

    }

    private JPanel outer;
    private JPanel buttonHolder;
    private JButton restoreDefault;
    private JButton cancel;
    private JButton finish;

    private JButton monthly;
    private JButton yearly;

    private Font f = UtilsV2.getFont();

    public GUISettings(){
        super();

        if(!backupDatabase())
            System.exit(0);
        Container con = getContentPane();

        UIManager.put("swing.boldMetal", Boolean.FALSE);

        outer = new JPanel();
        outer.setLayout(new GridLayout(2,2));
        finish = new JButton("Load from xls");
        cancel = new JButton("Cancel");
        restoreDefault = new JButton("Restore Default");

        monthly = new JButton("Monthly");
        yearly = new JButton("Yearly");
        buttonHolder = new JPanel();
        buttonHolder.setLayout(new GridLayout(2,2));
        buttonHolder.add(monthly);
        monthly.setFont(f);
        monthly.addActionListener(this);
        buttonHolder.add(yearly);
        yearly.setFont(f);
        yearly.addActionListener(this);

        outer.add(restoreDefault);
        outer.add(finish);
        outer.add(buttonHolder);
        outer.add(cancel);

        restoreDefault.addActionListener(this);
        finish.addActionListener(this);
        cancel.addActionListener(this);
        for (Component component : outer.getComponents()) {
            component.setFont(f);
        }

        con.add(outer);

        pack();
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);

        setVisible(true);


    }


    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if(source==cancel){
            dispose();
        }
        if(source==finish){
            int j = JOptionPane.showConfirmDialog(null, "Are you sure?", "Confirm model", JOptionPane.YES_NO_OPTION);

            if (j == 0) {
                finish();
                System.exit(0);
            }
        }
        if(source==restoreDefault){
            int j = JOptionPane.showConfirmDialog(null, "Are you sure you want to revert the system\n back to the default model?", "Confirm model", JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE);

            if (j == 0) {
                if(restoreDefault())
                    System.exit(0);

            }
        }
        if(source==monthly){
            String month = JOptionPane.showInputDialog(null,"Please enter month.");
            int monthI = -1;
            try{
               monthI = Integer.parseInt(month);
            } catch(Exception exception){
                JOptionPane.showMessageDialog(null, exception.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            String year = JOptionPane.showInputDialog(null,"Please enter year.(just the last two digits, eg  14) ");
            int yearI = -1;
            try{
                yearI = Integer.parseInt(year);
            } catch(Exception exception){
                JOptionPane.showMessageDialog(null,exception.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
                return;
            }

            SQLDatabase db = new SQLDatabase();
            db.monthly(monthI,yearI);
        }
        if(source==yearly){
            String year = JOptionPane.showInputDialog(null,"Please enter year.(just the last two digits, eg  14) ");
            int yearI = -1;
            try{
                yearI = Integer.parseInt(year);
            } catch(Exception exception){
                JOptionPane.showMessageDialog(null,exception.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
                return;
            }
            SQLDatabase db = new SQLDatabase();
            db.yearly(yearI);
        }

    }

    private void finish(){
        new ModelImport();
    }

    private boolean restoreDefault(){
        SQLDatabase db = new SQLDatabase();
        if(db.writeDefaultModel()) {
            JOptionPane.showMessageDialog(null, "Success!", "Success.", JOptionPane.INFORMATION_MESSAGE);
            return true;
        }
        return false;
    }
    private final String fileName = "DrinkPackageDatabase";

    private boolean backupDatabase(){
        File source  = new File("DrinkPackageDatabase.db");
        if(!source.exists()){
            JOptionPane.showMessageDialog(null,"No database found, creating default.","Creating new database",JOptionPane.INFORMATION_MESSAGE);
            restoreDefault();
        }

        File dest = new File("DrinkPackageDatabase.BAK");
        if(dest.exists()){
            int j = JOptionPane.showConfirmDialog(null,"A backup already exists. Would you like to overwrite it?","Backup exists",JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE);
            if(j == 0){
                dest.delete();
            }
        }

        try {
            Files.copy(source.toPath(),dest.toPath());
            JOptionPane.showMessageDialog(null,"Backup Created.","Success",JOptionPane.INFORMATION_MESSAGE);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            int j = JOptionPane.showConfirmDialog(null, "A backup of the database was not created.\nAre you sure you want to continue?", "A problem has occured", JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE);

            if (j == 0) {
                return true;
            }

            return false;
        }
    }


}
