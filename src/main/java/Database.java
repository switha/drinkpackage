
/**
 * Created by Switha on 3/17/14.
 * Interface for a standard database, which will be used for loading and storing objects
 */
public interface Database {



    /**
     *
     * @return returns all the categories (with items)
     */
    public ActionObject readModel();

    /**
     * checks whether a new month has arrived and if appropriate creates a monthly/yearly report
     * if a monthly or yearly report for the period exists, a new will not be created.
     */
    public void makeMonthlyAndYearlyReport();

    /**
     *
     * @param actionObject contains all the data to be saved
     */
    public void writeAction(ActionObject actionObject);


}
