import java.util.List;

/**
 * Created by Switha on 4/11/14.
 * Interface for a database used by an archive
 */
public interface ArchiveDatabase {
    /**
     *
     * @return a list of all actions objects in the archive
     */
    public List<ArchiveObject> readArchive();
}
