import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Switha on 4/12/14.
 * This is a helper class.
 * It opens the excel which is specified in the path and resizes the first 6 cols.
 */
public class AutoSizeExcel {

    /**
     * This is a helper class, it used to
     * @param file
     * @throws IOException
     */
    public AutoSizeExcel(File file) throws IOException {
        FileInputStream fis = new FileInputStream(file);
        HSSFWorkbook workbook = new HSSFWorkbook(fis);
        HSSFSheet sheet = workbook.getSheetAt(0);
        for (int i = 0; i < 32; i++) {
            sheet.autoSizeColumn(i);
        }
        fis.close();
        FileOutputStream fos = new FileOutputStream(file);
        workbook.write(fos);
        fos.close();


    }


}
