import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Switha on 5/29/14.
 */
public class YearlyReport {

    /**
     * processes all the actions supplied in @param and creates a list of items,
     * use getList method to retrieve it
     * @param inputList
     */
    public YearlyReport(List<ActionObject>[] inputList,int year) {

        List<Item>[] yearlyItems = new List[12];


        for (int i = 0; i < 12; i++) {
            yearlyItems[i] = processActions(inputList[i],false);
        }

        for (List<Item> yearlyItem : yearlyItems) {
            for (Item item : yearlyItem) {
                if(item.getAmountSold()>0){
                    System.out.println(item.getName()+": "+item.getAmountSold());
                }

            }
        }


        try {
            new CreateExcel(yearlyItems, false, false,0, year);
            new SendReport("Yearly_20"+year);
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "An Error has occured with the yearly report, please report this to your administrator!\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            UtilsV2.disablePrompt();

        }


        yearlyItems = new List[12];
        for (int i = 0; i < 12; i++) {
            yearlyItems[i] = processActions(inputList[i],true);
        }

        try{
            new CreateExcel(yearlyItems,true,false,0,year);
           new SendReport("Yearly_Other_20"+year);
        } catch (Exception e){
            e.printStackTrace();
             JOptionPane.showMessageDialog(null,"An Error has occured while creating the yearly report for Other, please report this to your administrator.\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            UtilsV2.disablePrompt();
        }


        UtilsV2.disablePrompt();

    }




    private List<Item> processActions(List<ActionObject> inputList,boolean other){
        List<Item> totalSold = new ArrayList<>();
        for (ActionObject inputActionObject : inputList) {

            for (Category inputCategory : inputActionObject.getCategories()) {
                if(inputCategory.getCategoryName().equals("Other")&&!other)
                    continue;
                if(!inputCategory.getCategoryName().equals("Other")&&other)
                    continue;
                inputItemLoop:
                for (Item inputItem : inputCategory.getItems()) {
                    //iterate through totalSold
                    for (Item finalItem : totalSold) {
                        if(finalItem.equals(inputItem)) {
                            finalItem.addAmount(inputItem.getAmountSold());
                            continue inputItemLoop;
                        }
                    }
                    //the item doesn't exist in total, otherwise we wouldn't get here. Add it
                    totalSold.add(inputItem);

                }
            }
        }
        return totalSold;
    }

}
