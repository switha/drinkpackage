import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;

/**
 * Created by Switha on 3/17/14.
 * Represents an item
 * Property change fires only on amount sold
 */
public class Item implements Serializable, Comparable<Item> {

    private static final long serialVersionUID = 36;

    private String name = "";
    private String packaging = "";
    private PropertyChangeSupport pcs = new PropertyChangeSupport(this);

    private int amountSold = 0;
    private boolean fullEdit = false;

    /**
     *
     * @param name Name of the item
     * @param packaging Packaging of the item
     */
    public Item(String name, String packaging){
        this.setName(name);
        this.setPackaging(packaging);
    }

    /**
     * creates a new item with all properties set to 0 or " "
     */
    public Item(){

    }

    /**
     * creates a new item with all properties set to 0 or " "
     * use @param to set whether when opening the item a full editor will be used
     * @param useFullEdit
     */
    public Item(boolean useFullEdit){
        this.fullEdit = useFullEdit;
    }

    /**
     *
     * @param name Name of the item
     * @param packaging Packaging of the item
     * @param amountSold The amount of this item that was sold
     */
    public Item(String name, String packaging, int amountSold){
        this(name,packaging);
        this.setAmountSold(amountSold);
    }


    /**
     *
     * @return Name - returns the name of the item
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @return packaging - returns the packaging if the item
     */
    public String getPackaging() {
        return packaging;
    }


    /**
     *
     * @return amountSold - returns the amount sold of the item
     */
    public int getAmountSold() {
        return amountSold;
    }

    /**
     *
     * @param amountSold - sets the amount sold to a NEW value
     */
    public void setAmountSold(int amountSold) {
        int old = this.amountSold;

        this.amountSold = amountSold;
        pcs.firePropertyChange("amountSold",old,amountSold);

    }

    /**
     *
     * @param amount - adds the increases the amount sold of the item by the parameter -amount
     */

    public void addAmount(int amount){
        int old = this.getAmountSold();
        this.setAmountSold(this.getAmountSold() + amount);
        pcs.firePropertyChange("amountSold", old, getAmountSold());

    }

    /**
     *
     * @param listener adds a property change listener to the item - listens for changes in amount
     */
    public void addPropertyChangeListener(PropertyChangeListener listener){
        pcs.addPropertyChangeListener(listener);
    }

    /**
     * removes the property change listener in @param
     * @param listener
     */
    public void removePropertyChangeListener(PropertyChangeListener listener){
        pcs.removePropertyChangeListener(listener);
    }

    /**
     * removes all property change listeners from the item
     */
    public void removeAllPropertyChangeListeners(){
        for (PropertyChangeListener propertyChangeListener : pcs.getPropertyChangeListeners()) {
            removePropertyChangeListener(propertyChangeListener);
        }
    }

    /**
     * Sets the value of amount sold to 0
     */
    public void normalize(){
        if(getAmountSold()<0)setAmountSold(0);
    }

    /**
     * @return true if the item should be edited fully (e.g. it is an item from other
     */
    public boolean isFullEdit(){
        return fullEdit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Item item = (Item) o;

        if (!getName().equals(item.getName())) return false;
        if (!getPackaging().equals(item.getPackaging())) return false;

        return true;
    }



    @Override
    public int hashCode() {
        int result = getName() != null ? getName().hashCode() : 0;
        result = 31 * result + (getPackaging() != null ? getPackaging().hashCode() : 0);
        return result;
    }



    /**
     * Sets the name of the item to @param
     * @param name
     */
    public void setName(String name) {
        this.name = name;

    }

    /**
     * sets the packaging of the item to @param
     * @param packaging
     */
    public void setPackaging(String packaging) {
        this.packaging = packaging;

    }


    @Override
    public int compareTo(Item o) {
        return o.getName().compareTo(getName());
    }
}
