import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;

/**
 * Created by Swith on 18/4/2014.
 * GUI for the final check
 * allows brosing and editing all the filled out items and finally creating/sending a report
 */
public class GUIFinalCheck extends JFrame implements ActionListener,PropertyChangeListener {

    //I've absolutely no idea why it needs this ...
    private static final long serialVersionUID = 35;


    private ActionObject actionObject;
    private JButton confirm = new JButton("Send");
    private JButton cancel = new JButton("Cancel");

    private JPanel outer;
    private JPanel navigation;

    private JButton left = new JButton("<");
    private JButton right = new JButton(">");
    private JButton back = new JButton("^");

    private int page = 0;
    private final int pages;
    private java.util.List<Item> items;

    private final int itemsPerPage = 17;

    private JButton[] buttons = new JButton[itemsPerPage]; //changed

    private final Font f = UtilsV2.getFont();

    /**
     * Allows browsing through all the items that have a >0 amount, and allows sending reports (creation)
     * @param actionObject the action which is to be checked respectively sent
     */
    public GUIFinalCheck(ActionObject actionObject) {
        super("Final check");
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        System.out.println("^^ OPENING FINAL CHECK ");
        addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent we)
            {
                String ObjButtons[] = {"Quit","Cancel"};
                int PromptResult = JOptionPane.showOptionDialog(null,
                        "Are you sure you want to exit?\nYou can open the action again\n when you start DrinkPackage again.",
                        "Are you sure?",
                        JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null,
                        ObjButtons,ObjButtons[1]);
                if(PromptResult==0)
                {
                    System.exit(0);
                }
            }
        });


        this.actionObject = actionObject;

        items = actionObject.getFilledItems();
        addPropertyListeners();

        left.setEnabled(false);

        navigation=new JPanel();
        navigation.setLayout(new GridLayout(1, 3));
        navigation.add(left);
        left.setFont(f);
        navigation.add(back);
        back.setFont(f);
        navigation.add(right);
        right.setFont(f);
        left.addActionListener(this);
        right.addActionListener(this);
        back.addActionListener(this);


        pages = (items.size() / itemsPerPage)+1;

        setButtons();


    }

    private void setButtons() {
        outer=new JPanel();


        outer.setLayout(new GridLayout(6,3));
        if(page==pages){
            outer.add(confirm);
            outer.add(cancel);
            confirm.addActionListener(this);
            cancel.addActionListener(this);
            confirm.setFont(f);
            cancel.setFont(f);
        }else {
            int start = page * itemsPerPage;

            for (int i = start; i < start + itemsPerPage; i++) {

                if (i < items.size()) {
                    JButton temp = new JButton(items.get(i).getName()+" - "+items.get(i).getAmountSold()+" ("+items.get(i).getPackaging()+")");
                    buttons[i - start] = temp;
                    temp.addActionListener(this);
                    outer.add(temp);
                    temp.setFont(f);
                } else outer.add(new JLabel());

            }
        }
        outer.add(navigation);
        setContentPane(outer);

        revalidate();
        repaint();

        setVisible(true);
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);


    }



    private void performEndActions(){

        Database db = new SQLDatabase();
        actionObject.normalize();
        db.writeAction(actionObject);
        try {
            System.out.println("Exclelling");
            new CreateExcel(actionObject);
            System.out.println("Pdfing");
            new CreatePDF(actionObject );
            System.out.println("Sending");
            new SendReport(actionObject);

        } catch (Exception e) {
            UtilsV2.disablePrompt();
            JOptionPane.showConfirmDialog(null,e.getMessage()+"\nIf relevant, you may reopen the application\n and try again.","Error",JOptionPane.DEFAULT_OPTION,JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        }
        deleteTmp();
        UtilsV2.disablePrompt();
        JOptionPane.showConfirmDialog(null,"Report created and sent.","Success",JOptionPane.DEFAULT_OPTION,JOptionPane.INFORMATION_MESSAGE
        );
        System.exit(0);
    }

    /*
    deletes the tmp object
     */
    private void deleteTmp() {
        File f = new File("ActionObject.tmp");
        if(f.exists());
        f.delete();
    }


    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        Object source = actionEvent.getSource();
        if(source==back){
            dispose();
        }
        if(source==left){
            if(page>0) page--;
            if(page==0)left.setEnabled(false);
            if(page<pages) right.setEnabled(true);
            setButtons();
            return;
        }
        if(source==right){
             if(page<pages) page++;
            if(page==pages) right.setEnabled(false);
            if(page>0) left.setEnabled(true);
            setButtons();
            return;
        }
        for (int i = 0; i < buttons.length; i++) {
            if(source==buttons[i]){
               Item temp = items.get(i+page*8);
                if(temp.isFullEdit()) new GUIFullEdit(temp);
               else new GUISetItemAmount(items.get(i+page*8),actionObject);

               return;
            }
        }
        if(source==cancel) {
            dispose();
        }
        if(source==confirm) {
            if(actionObject.getName()==null||actionObject.getName().equals("")||actionObject.getAuthor()==null|actionObject.getAuthor().equals("")) {
                JOptionPane.showConfirmDialog(null, "You have to enter an action and author name!\nPlease return to the main menu and fill them out.", "Cannot continue", JOptionPane.OK_OPTION, JOptionPane.WARNING_MESSAGE);
                return;
            }
            if(0==JOptionPane.showConfirmDialog(null,"Are you sure?","Send?",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE)){

            UtilsV2.showPrompt();
            performEndActions();
            }
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        System.out.println("Setting them buttons...");
        setButtons();
    }


    private void addPropertyListeners(){
        for (Item item : items) {
            item.removeAllPropertyChangeListeners();
            item.addPropertyChangeListener(this);
        }
    }
}
