/**
 * Created by Switha on 4/7/14.
 * The main database
 */

import javax.swing.*;
import java.io.*;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SQLDatabase implements Database, ArchiveDatabase {


    private final String fileName = "DrinkPackageDatabase.db";

    private static SimpleDateFormat sdfMM = new SimpleDateFormat("MM");
    private static SimpleDateFormat sdfy = new SimpleDateFormat("yy");
    private static SimpleDateFormat sdfdd = new SimpleDateFormat("dd");


    /**
     * constructs an SQL database worker
     * if the database file does not exist, a new one is created with the model
     *
     */
    public SQLDatabase(){

        File f = new File(fileName); //if there is no database, create a new one and store the default item selection
        if(!f.exists()) writeDefaultModel();


    }



    public static void main(String[] args) throws SQLException, IOException, ClassNotFoundException {

        System.out.println("> Opening DB");
        SQLDatabase db = new SQLDatabase();
        System.out.println("< DB Open");

        List e = db.readAll();
        System.out.println(e.size());


    }


    @Override
    public void makeMonthlyAndYearlyReport() {
        Connection c = null;
        try {
            c = getConnection();
            String maxID = "SELECT MAX(ID) FROM ACTIONS";

            PreparedStatement pstmt = c.prepareStatement(maxID);
            ResultSet rs = pstmt.executeQuery();
            rs.next();
            int max = rs.getInt(1);
            rs.close();
            ActionObject actionObject = readObject(max);
            Date currentDate = new Date();

            String currentMonth = sdfMM.format(currentDate);
            String lastMonth = sdfMM.format(actionObject.getDate());
            String currentYear = sdfy.format(currentDate);
            String lastYear = sdfy.format(actionObject.getDate());
            int year = Integer.parseInt(lastYear);

            if(lastMonth.compareTo(currentMonth) < 0){
                UtilsV2.showPrompt();
                System.out.println("Making monthly ...");
                int month = Integer.parseInt(lastMonth);
                new MonthlyReport(readMonth(month),month,year);
                UtilsV2.disablePrompt();

            }
            if(lastYear.compareTo(currentYear) < 0){
                UtilsV2.showPrompt();
                System.out.println("Making yearly ...");
                new YearlyReport(readYear(year),year);
                UtilsV2.disablePrompt();

            }

        } catch (Exception e) {
            e.printStackTrace();
            UtilsV2.disablePrompt();
        }
    }

    public void monthly(int month, int year){

        try {
            new MonthlyReport(readMonth(month),month,year);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,e.getMessage()+"\n"+e.getCause(),"Error",JOptionPane.ERROR_MESSAGE);
       }
    }
    public void yearly(int year){

        try{
            new YearlyReport(readYear(year),year);
        } catch (Exception e){
            JOptionPane.showMessageDialog(null,e.getMessage()+"\n"+e.getCause(),"Error",JOptionPane.ERROR_MESSAGE);

        }
    }


    //creates a connection
    private Connection getConnection() throws SQLException, ClassNotFoundException {
        Class.forName("org.sqlite.JDBC");
        return DriverManager.getConnection("jdbc:sqlite:"+fileName);

    }

    private List<ActionObject>[] readYear(int year) throws SQLException, ClassNotFoundException, IOException {
        List<ActionObject>[] yearActions = new List[12];

        Connection c = getConnection();
        for(int i = 1; i < 13; i ++) {
            String sql = "SELECT DATA FROM ACTIONS WHERE YEAR = ? AND MONTH = ?";
            PreparedStatement pstmt = c.prepareStatement(sql);
            pstmt.setInt(1, year);
            pstmt.setInt(2,i);
            ResultSet rs = pstmt.executeQuery();

            List<ActionObject> inner = new ArrayList<>();
            while (rs.next()) {

                byte[] buffer = rs.getBytes(1);
                ObjectInputStream ois = null;
                if (buffer != null)
                    ois = new ObjectInputStream(new ByteArrayInputStream(buffer));

                Object deSerialized = ois.readObject();
                inner.add((ActionObject) deSerialized);

            }
            yearActions[i-1] = inner;
            rs.close();
            pstmt.close();
        }

        c.close();


        return yearActions;
    }

    private List<ActionObject>[] readMonth(int month) throws SQLException, ClassNotFoundException, IOException {
        System.out.println(">>Reading month in db");
       

        List<ActionObject>[] monthActions = new List[31];

        Connection c = getConnection();

        for(int i = 1; i <32; i ++){
            String sql = "SELECT DATA FROM ACTIONS WHERE MONTH = ? AND DAY = ?";
            PreparedStatement pstmt = c.prepareStatement(sql);
            pstmt.setInt(1,month);
            pstmt.setInt(2,i);
            ResultSet rs = pstmt.executeQuery();

            List<ActionObject> inner = new ArrayList<>();
            while(rs.next()){

                byte[] buffer = rs.getBytes(1);
                ObjectInputStream ois = null;
                if (buffer != null)
                    ois = new ObjectInputStream(new ByteArrayInputStream(buffer));

                Object deSerialized = ois.readObject();
                inner.add((ActionObject) deSerialized);

            }
            monthActions[i-1]=inner;
            rs.close();
            pstmt.close();
        }
        c.close();

        /*
        int apples = 0;
        int appleObjects = 0;
        for (List<ActionObject> yearAction : monthActions) {
            for (ActionObject actionObject : yearAction) {
                for (Category category : actionObject.getCategories()) {
                    for (Item item : category.getItems()) {
                        if(item.getName().equals("Apple")){
                            appleObjects++;
                            apples += item.getAmountSold();
                        }
                    }
                }

            }
        }
        System.out.printf("There are %d appleObjects in SQL\nApples Total in SQL: %d\n", appleObjects, apples);
        */
        System.out.println("<<Returning month");
        return monthActions;
    }

    //reads object at position
    private ActionObject readObject(int id) throws SQLException, IOException, ClassNotFoundException {
        Connection c = getConnection();
        String sql = "SELECT DATA FROM ACTIONS WHERE ID = ?";
        PreparedStatement pstmt = c.prepareStatement(sql);
        pstmt.setInt(1,id);
        ResultSet rs = pstmt.executeQuery();
        rs.next();
        byte[] buffer = rs.getBytes(1);
        ObjectInputStream ois =null;
        if(buffer!=null)
            ois = new ObjectInputStream(new ByteArrayInputStream(buffer));

        Object deSerialized = null;

        try {
            deSerialized = ois.readObject();
        } catch (InvalidClassException e){
            e.printStackTrace();
            JOptionPane.showMessageDialog(null,"Database and current system not compatible.\n Contact your System administrator!\nThe System cannot run!\n"+e.getMessage()+"\n"+e.getCause(),"Critical Error!",JOptionPane.ERROR_MESSAGE);
            System.exit(5);
        }

        rs.close();
        pstmt.close();
        c.close();
        return  (ActionObject) deSerialized;

    }

    private List<ArchiveObject> readAll() throws SQLException, ClassNotFoundException, IOException {
        Connection c = getConnection();

        String query = "SELECT COUNT(*) FROM Actions";
        PreparedStatement pstmt  = c.prepareStatement(query);
        ResultSet rs = pstmt.executeQuery();
        rs.next();
        int size = rs.getInt(1);


        List<ArchiveObject> archiveObjects = new ArrayList<>();


        query = "SELECT Data, Id FROM Actions";
         pstmt = c.prepareStatement(query);
        rs = pstmt.executeQuery();


        int id;
        ArchiveObject temp;
        while(rs.next()){
            byte[] buffer  = rs.getBytes(1);
            ObjectInputStream ois = null;
            if(buffer!=null) {
                ois = new ObjectInputStream(new ByteArrayInputStream(buffer));
                Object deSerialized = ois.readObject();
                id = rs.getInt(2);
               temp = new ArchiveObject((ActionObject) deSerialized,id);
                archiveObjects.add(temp);
            }

        }
        rs.close();
        pstmt.close();
        c.close();

        if(size == archiveObjects.size())
            System.out.println(size + ": Yay it works!");

        return archiveObjects;


    }


    //reads all
    private List<ActionObject> readAllBAD() throws SQLException, IOException, ClassNotFoundException {
        Connection c = getConnection();
        String maxID = "SELECT MAX(ID) FROM ACTIONS";

        PreparedStatement pstmt = c.prepareStatement(maxID);
        ResultSet rs = pstmt.executeQuery();
        rs.next();
        int max = rs.getInt(1);

        List<ActionObject> actionObjects = new ArrayList<>();

        String sql = "SELECT Data, FROM ACTIONS WHERE ID = ?";
        for (int i = 1; i <= max; i++) {
            pstmt = c.prepareStatement(sql);
            pstmt.setInt(1,i);
            rs = pstmt.executeQuery();
            rs.next();
            byte[] buffer = rs.getBytes(1);
            ObjectInputStream ois =null;
            if(buffer!=null)
                ois = new ObjectInputStream(new ByteArrayInputStream(buffer));

            Object deSerialized = ois.readObject();
            rs.close();

            pstmt.close();
            actionObjects.add((ActionObject) deSerialized);

        }

        c.close();

        return actionObjects;
    }

    private void writeObject(ActionObject actionObject, int position) throws SQLException, IOException, ClassNotFoundException {
        Connection c = getConnection();

        String check = "DELETE FROM ACTIONS WHERE ID IS (?)";
        PreparedStatement pstmt = c.prepareStatement(check);
        pstmt.setInt(1,0);
        pstmt.executeUpdate();
        System.out.println("Deleted "+position);

        String write = "INSERT INTO ACTIONS (month,year,DATA,id,day) VALUES (?,?,?,?,?)";
         pstmt = c.prepareStatement(write);


        pstmt.setInt(1,(Integer.parseInt(sdfMM.format(actionObject.getDate()))));
        pstmt.setInt(2,(Integer.parseInt(sdfy.format(actionObject.getDate()))));
        pstmt.setBytes(3, getByteArray(actionObject));
        pstmt.setInt(4, position);
        pstmt.setInt(5,(Integer.parseInt(sdfdd.format(actionObject.getDate()))));
        pstmt.executeUpdate();

        /*
        ResultSet rs = pstmt.getGeneratedKeys();

        int serialized_id = -1;
        if(rs.next()){
            serialized_id=rs.getInt(1);
        }

        rs.close();
        */
        pstmt.close();
        c.close();
    }

    /**
     * @param id delets the whole row with supplied id
     */
    public boolean delete(int id) {
        Connection c = null;
        try {
            c = getConnection();

        String delete = "DELETE FROM actions WHERE ID IS (?)";
        PreparedStatement pstmt = c.prepareStatement(delete);
        pstmt.setInt(1, id);
        pstmt.executeUpdate();

        pstmt.close();
        c.close();

            JOptionPane.showMessageDialog(null,"Success!","",JOptionPane.INFORMATION_MESSAGE);
            return true;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage(),"Error!",JOptionPane.ERROR_MESSAGE);
            return false;
        }



    }

    //writes actionobject to new slot
    private  void writeObject( ActionObject actionObject) throws SQLException, IOException, ClassNotFoundException {
        Connection c = getConnection();


        removePropertyChangeListener(actionObject);
        String write = "INSERT INTO ACTIONS (month,year,day,DATA) VALUES (?,?,?,?)";
         PreparedStatement pstmt = c.prepareStatement(write);


        pstmt.setInt(1,(Integer.parseInt(sdfMM.format(actionObject.getDate()))));
        pstmt.setInt(2,(Integer.parseInt(sdfy.format(actionObject.getDate()))));
        pstmt.setInt(3, (Integer.parseInt(sdfdd.format(actionObject.getDate()))));
        pstmt.setBytes(4, getByteArray(actionObject));
        pstmt.executeUpdate();


        ResultSet rs = pstmt.getGeneratedKeys();
        int serialized_id = -1;
        if(rs.next()){
            serialized_id=rs.getInt(1);
        }
        rs.close();
         pstmt.close();
        c.close();
       System.out.println("Serialized, pos: "+serialized_id);
    }

    private void updateDB() throws SQLException, ClassNotFoundException, IOException {

        //get all data into memory
       List<ArchiveObject> databaseContents = readAll();

        File f = new File("DrinkPackageDatabase.db");

        f.delete();
        f = new File("DrinkPackageDatabaseNew.db");


        writeDefaultModel();
        for (ArchiveObject databaseContent : databaseContents) {
            System.out.println(databaseContent.getActionObject().getName());
            writeObject(databaseContent.getActionObject());

        }



        System.out.println("DB Updated");

    }

        //writeObject helper class, creates blob
        private  byte[] getByteArray(Object object) throws IOException, SQLException {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(object);
            oos.flush();
            oos.close();

            byte[] b = baos.toByteArray();
            baos.flush();
            baos.close();
            return b;

        }
    //creates a new table
    private  void createTable() throws SQLException, ClassNotFoundException {
        Connection c = getConnection();
        Statement stmt = c.createStatement();
        String sql = "CREATE TABLE IF NOT EXISTS ACTIONS (id INTEGER PRIMARY KEY AUTOINCREMENT ," +
                "day INTEGER," +
                "month INTEGER," +
                "year INTEGER," +
                "data BLOB NOT NULL);";

        stmt.executeUpdate(sql);
        stmt.close();
        c.close();
    }


    /**
     * Writes the supplied actionObject as the default model to the database
     * Database stays untouched
     * @param actionObject
     */
    public boolean writeDefaultModel(ActionObject actionObject){
        try {
            System.out.println("Writing default");

            createTable();
            writeObject(actionObject,0);

            return true;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Error writing model!\n"+e.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
            return false;
        }

    }

    /**
     * Restores the model to factory settings
     * Database stays untouched
     */
    public boolean writeDefaultModel(){

        Database db = new DummyDatabase();
        return writeDefaultModel(db.readModel());

    }



    @Override
    public ActionObject readModel() {
        ActionObject a=null;
        try {
            a =readObject(0);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return a;
    }

    @Override
    public void writeAction(ActionObject actionObject) {
        try {
            writeObject(actionObject);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<ArchiveObject> readArchive() {
        List<ArchiveObject> archiveObjects =new ArrayList<>();

        try{
            archiveObjects = readAll();
        }
         catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return archiveObjects;
    }

    //Removes the property listeners from objects
    //Use only when you don't need to set the GUI with it!
    private void removePropertyChangeListener(ActionObject action){
        for (Category category : action.getCategories()) {
            for (Item item : category.getItems()) {
                item.removeAllPropertyChangeListeners();
            }
        }
    }

}
