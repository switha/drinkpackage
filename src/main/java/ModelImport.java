/**
 * Created by Switha on 5/17/14.
 */


import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * IMPORTS THE MODEL FROM THE EXCEL FILE
 */
public class ModelImport {

    private String fileName = "model.xls";
    private Workbook workBook;
    private Sheet sheet;

    ActionObject model;


    public ModelImport(){
        try {
            openConnection();
            workBook();
            writeModel();
            JOptionPane.showMessageDialog(null,"Success.","Success",JOptionPane.INFORMATION_MESSAGE);
        }catch (Exception e){
            JOptionPane.showMessageDialog(null,e.getMessage(),"An error has occured",JOptionPane.ERROR_MESSAGE);
        }

    }
    private void openConnection() throws IOException, BiffException {
        File input = new File(fileName);

        System.out.println("Opening workbook");
        workBook = Workbook.getWorkbook(input);
        System.out.println("Opening sheet");
        sheet = workBook.getSheet(0);
        System.out.println("Connection Opened");

    }
    private void workBook(){



        List<Category> categories = new ArrayList<>();
        List<Item> items;

        Cell categoryC = sheet.getCell(0,0);
        System.out.println(categoryC.getContents());
        Cell itemC;
        Cell packagingC;


        int i = 0;
        while(!categoryC.getContents().equals("end")){
            
            items = new ArrayList<>();
            itemC = sheet.getCell(1,i);
            packagingC = sheet.getCell(2,i);
            while(!itemC.getContents().equals("end")){
                items.add(new Item(itemC.getContents(),packagingC.getContents()));
                i++;
                itemC = sheet.getCell(1,i);
                packagingC = sheet.getCell(2,i);
            }

            categories.add(new Category(categoryC.getContents(),items));
            i++;
            categoryC = sheet.getCell(0,i);

        }
        model = new ActionObject(new Date(),"model",categories);
        model.printSelf();
    }

    private void writeModel(){
        SQLDatabase db = new SQLDatabase();
        db.writeDefaultModel(model);
    }

}
