import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

/**
 * Created by Swith on 7/5/2014.
 * used to add other items
 */


public class GUIOther extends JFrame implements ActionListener, PropertyChangeListener{

    private static final long serialVersionUID = 35;


    private ActionObject actionObject;
    private List<Item> items;

    private JPanel outer;


    private JButton back = new JButton("Back");


    private JButton[] buttons = new JButton[8];

    final private Font f = UtilsV2.getFont();


    /**
     * creates a gui for browsing items of category, creates a 3x3 panel with buttons
     *
     */
    public GUIOther() {
        super("Other");
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent we)
            {
                String ObjButtons[] = {"Quit","Cancel"};
                int PromptResult = JOptionPane.showOptionDialog(null,
                        "Are you sure you want to exit?\nYou can open the action again\n when you start DrinkPackage again.",
                        "Are you sure?",
                        JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null,
                        ObjButtons,ObjButtons[1]);
                if(PromptResult==0)
                {
                    System.exit(0);
                }
            }
        });

        this.actionObject = UtilsV2.getAction();


        back.setFont(f);
        back.addActionListener(this);

        createOtherCategory();
        getOtherCategory();
        addPropertyListeners();
        putKbd();
        setButtons();


    }

    private void createOtherCategory(){
        actionObject = UtilsV2.getAction();
        for (Category category1 : actionObject.getCategories()) {
            if(category1.getCategoryName().equals("Other"))
                return;
        }

        Category other = new Category("Other");

        for (int i = 0; i < 8; i++) {
            Item newItem = new Item(true);
            other.addItem(newItem);
        }

        actionObject.addCategory(other);
    }

    private void getOtherCategory(){
        for (Category category1 : actionObject.getCategories()) {
            if(category1.getCategoryName().equals("Other"))
                items = category1.getItems();
        }
    }

    private void setButtons() {
        outer=new JPanel();


        outer.setLayout(new GridLayout(3,3));


        for (int i = 0; i < 8 ; i++) {

            if (i < items.size()) {
                int sold = items.get(i).getAmountSold();
                JButton temp;
                if(sold<1) temp = new JButton("+");
                else temp = new JButton(items.get(i).getName() + " - " + sold + " (" + items.get(i).getPackaging() + ")");
                buttons[i] = temp;
                temp.addActionListener(this);
                outer.add(temp);
                temp.setFont(f);
            }
        }
        outer.add(back);
        setContentPane(outer);

        revalidate();
        repaint();

        setVisible(true);
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
    }



    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        Object source = actionEvent.getSource();
        if(source==back){
            new GUIMainMenu(actionObject);
            dispose();
        }

        for (int i = 0; i < buttons.length; i++) {
            if(source==buttons[i]){
                System.out.println(i);
                System.out.println(items.get(i).getName());

                new GUIFullEdit(items.get(i));
                return;
            }
        }
    }

    private void putKbd(){
        Action enterAction = new AbstractAction() {
            private static final long serialVersionUID = 35;

            @Override
            public void actionPerformed(ActionEvent actionEvent){
                new GUIMainMenu(actionObject);
                dispose();
            }
        };

        getRootPane().getInputMap(JComponent.WHEN_FOCUSED).put(KeyStroke.getKeyStroke(
                        KeyEvent.VK_ENTER, 0),
                "check");getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(
                        KeyEvent.VK_ENTER, 0),
                "check");

        getRootPane().getActionMap().put("check",enterAction);

    }


    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        //Object source = propertyChangeEvent.getSource();
        setButtons();
    }

    private void addPropertyListeners(){
        for (Item item : items) {
            item.removeAllPropertyChangeListeners();
            item.addPropertyChangeListener(this);
        }
    }

}
