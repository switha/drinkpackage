import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by Switha on 4/11/14.
 * creates a pdf from the supplied actionObject
 */

public class CreatePDF {
    private File file;
    private ActionObject actionObject;
    private SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy");

    /*
    Defines the two fonts for the reportt
     */
    private final Font BOLD =
            new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
    private final Font NORMAL =
            new Font(Font.FontFamily.TIMES_ROMAN, 12);


    private final int MAXROWS = 40;




    /**
     * creates a pdf from the actionObject
     * @param actionObject  the object from which the pdf is to be created
     * @throws FileNotFoundException
     * @throws DocumentException
     */
    public CreatePDF(ActionObject actionObject) throws FileNotFoundException, DocumentException {
        this.actionObject = actionObject;

        file=new File(actionObject.getFileName()+".pdf");
        if(file.exists()){
            file.delete();
        }
        Document document = new Document(PageSize.A4,50,50,50,50);
        PdfWriter.getInstance(document, new FileOutputStream(file));
        document.open();

        PdfPTable table = createTable();

        document.add(table);
        document.add(createDataTable());
        document.close();
    }

    private PdfPTable createTable() throws DocumentException {
        PdfPTable table = new PdfPTable(1);


        PdfPCell cell;
        cell = b("Fill this table carefully and hand in with the Daily Report");

        table.addCell(cell);

        cell=b("Date: "+sdf.format(actionObject.getDate())+" Group Name: "+ actionObject.getName()+" Created by: "+ actionObject.getAuthor());
        table.addCell(cell);



        return table;
    }

    //we want two cols with items, so we create holder and the insert left and right
    private PdfPTable createDataTable() throws DocumentException {
        PdfPTable holder = new PdfPTable(2);
        float[] width = new float[] {1f,1f};
        holder.setWidths(width);
        PdfPTable[] dataTables = left();
        holder.addCell(dataTables[0]);
        holder.addCell(dataTables[1]);

        return holder;
    }


    private PdfPTable[] left() throws DocumentException {
        PdfPTable[] tables = new PdfPTable[2];
        PdfPTable table = new PdfPTable(3);
        float[] widths = new float[] {200f,500f,150f};
        table.setWidths(widths);
        table.addCell(b("Amt:"));
        table.addCell(b("Name:"));
        table.addCell(b("Pkg:"));

        int index;

        int row =3;


        actionObject.normalize();
        List<Category> categories = actionObject.getCategories();
        int position = 0;
        outer:
        for ( index = 0; index <categories.size(); index++) {
            Category cat = categories.get(index);
            List<Item> items = cat.getItems();



            table.addCell(c(" "));
            table.addCell(b(cat.getCategoryName()));
            table.addCell(c(""));
            for (int j = 0; j < items.size(); j++) {
                position = j;
                row++;
                if(row >= MAXROWS) break outer;
                Item item = items.get(j);
                int amount = item.getAmountSold();
                table.addCell(Integer.toString(amount));
                table.addCell(c(item.getName()));
                table.addCell(c(item.getPackaging()));


            }
            row++;//addItem a blank
            for (int i = 0; i < 3; i++) {
                table.addCell(" ");//addItem a blank row
            }


        }


        tables[0]=table;
        tables[1]=right(index, position);
        return tables;

    }


    private PdfPTable right(int index, int position) throws DocumentException {
        PdfPTable table = new PdfPTable(3);
        float[] widths = new float[] {200f,500f,150f};
        table.setWidths(widths);
        table.addCell(b("Amt:"));
        table.addCell(b("Name:"));
        table.addCell(b("Pkg:"));
        List<Category> categories = actionObject.getCategories();
        int first = index;
        for (; index <categories.size(); index++) {
            Category cat = categories.get(index);
            List<Item> items = cat.getItems();


            table.addCell(c(""));
            boolean other = false;
            if(cat.getCategoryName().equals("Other"))
                other = true;
            if(index==first) {
                table.addCell(b(cat.getCategoryName()+" (ctd)"));
            } else table.addCell(b(cat.getCategoryName()));

            table.addCell(c(""));
            for (int j = (index==first)?position:0; j < items.size(); j++) {

                    Item item = items.get(j);
                    int amount = item.getAmountSold();
                if(!other||(other&&amount>0)) {
                    table.addCell(Integer.toString(amount));
                    table.addCell(c(item.getName()));
                    table.addCell(c(item.getPackaging()));
                }



            }
            for (int i = 0; i < 3; i++) {
                table.addCell(" ");//addItem a blank row
            }


        }
        return table;
    }


    private PdfPCell c(String s){
        return new PdfPCell(new Phrase(s,NORMAL));
    }
    private PdfPCell b(String s){
        return new PdfPCell(new Phrase(s,BOLD));
    }




}
