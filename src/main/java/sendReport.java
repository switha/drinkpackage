import com.itextpdf.text.DocumentException;
import jxl.write.WriteException;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.*;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Scanner;

/**
 * Created by Switha on 4/13/14.
 * sends a report containing a pdf and xls report of the supplied actionObject
 */
public class SendReport {

    private String bodyText = " Here are the attachments";


    private Properties properties;
    private Session session;

    private ArrayList<String> emailAdresses=new ArrayList<>();
    private InternetAddress from;
    private String host;
    private String userName;
    private String password;
    private String subject;
    private boolean authenticate;


    private Address[] adresses;

    public static void main(String[] args) {
        Database db = new SQLDatabase();
        try {
            new SendReport(db.readModel());
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (WriteException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sends a new report which expects a xls file with <@param fileName>.xls to exist
     * @param fileName
     * @throws MessagingException
     */
    public SendReport(String fileName) throws MessagingException
    {
        File config = new File("mail.cfg");
        if (!config.exists()) {
            JOptionPane.showMessageDialog(null, "No mail settings found! \nContact administrator!", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
        readConfig(config);
        makeSession();
        sendMail(createMessage(fileName));
    }

    /**
     * sends a new report that contains the pdf and excel.
     * @param actionObject the object the report is to be generated from (contains fileName)
     *
     * @throws MessagingException exceptions, you get it
     * @throws WriteException
     * @throws DocumentException
     * @throws IOException
     */
    public SendReport(ActionObject actionObject) throws MessagingException, WriteException, DocumentException, IOException {
        File config = new File("mail.cfg");
        if (!config.exists()) {
            JOptionPane.showMessageDialog(null, "No mail settings found! \nContact administrator!", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }


        readConfig(config);

        makeSession();
        sendMail(createMessage(actionObject));

       // connectAndSend(actionObject);




    }



    private void sendMail(Message message) throws MessagingException {
        Transport t;
        /*
        if(authenticate) {
             t = (SMTPTransport) session.getTransport("smtp");
            t.connect(host,userName,password);


        } else {
             t = (SMTPTransport) session.getTransport("smtp");
            t.connect();
        }
        */
        t = session.getTransport();
        t.connect();
        t.sendMessage(message, adresses);
        t.close();

    }
    private void readConfig(File config){
        Scanner sc = null;
        try {
            sc = new Scanner(new FileReader(config));

            String[] params = new String[7];
            for (int i = 0; i < 7; i++) {
                params[i]=sc.nextLine();
            }

            host=params[0];
            from=new InternetAddress(params[1]);
            subject=params[2];
            bodyText=params[3];
            authenticate = Boolean.parseBoolean(params[4]);
            userName=params[5];
            password=params[6];
            while(sc.hasNextLine())
                emailAdresses.add(sc.nextLine());




        } catch (Exception e) {
            e.printStackTrace();
        } finally{
            if(sc!=null)
            sc.close();
        }
    }
        private void makeSession() {
            properties = new Properties();

            if (authenticate) {

                properties.put("mail.smtp.host", host);
                properties.put("mail.smtp.auth", "true");
                properties.put("mail.smtp.starttls.enable","true");
                properties.put("mail.transport.protocol", "smtp");
                properties.put("mail.smtp.localhost","localhost");


                Authenticator auth = new Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(userName, password);
                    }
                };
                session = Session.getInstance(properties, auth);

            } else {
                properties.put("mail.smtp.host", host);
                properties.put("mail.smtp.auth", "false");
                properties.put("mail.transport.protocol", "smtp");
                properties.put("mail.smtp.localhost","localhost");


                session = Session.getInstance(properties);
            }
            System.out.println("Hostname: "+session.getProperty("mail.smtp.host"));

        }

    private Message createMessage(ActionObject actionObject) throws MessagingException, DocumentException, IOException, WriteException {

            Message message = new MimeMessage(session);
            message.setFrom(from);
             adresses = new Address[emailAdresses.size()];
        System.out.println(adresses.length);
            for (int i = 0; i <adresses.length ; i++) {
                System.out.println("Adding recipient");
                adresses[i]=new InternetAddress(emailAdresses.get(i));
            }
            message.addRecipients(Message.RecipientType.TO, adresses);
            message.setSubject(subject);
            BodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setText(bodyText);
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);

            String fileNm = actionObject.getFileName();
            //make sure the files are there
            File file = new File(fileNm+".xls");
            if(!file.exists()) new CreateExcel(actionObject);
            file = new File(fileNm+".pdf");
            if(!file.exists()) new CreatePDF(actionObject);

            messageBodyPart=new MimeBodyPart();
            DataSource source = new FileDataSource(fileNm+".xls");
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(fileNm+".xls");
            multipart.addBodyPart(messageBodyPart);

            messageBodyPart=new MimeBodyPart();
            source = new FileDataSource(fileNm+".pdf");
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(fileNm + ".pdf");
            multipart.addBodyPart(messageBodyPart);

            message.setContent(multipart);
            return message;

    }

    //sends only excel
    private Message createMessage(String fileName) throws MessagingException {
        Message message = new MimeMessage(session);
        message.setFrom(from);
        adresses = new Address[emailAdresses.size()];
        for (int i = 0; i <adresses.length ; i++) {
            adresses[i]=new InternetAddress(emailAdresses.get(i));
        }
        message.addRecipients(Message.RecipientType.TO, adresses);
        message.setSubject(subject);
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setText(bodyText);
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);


        messageBodyPart=new MimeBodyPart();
        DataSource source = new FileDataSource(fileName+".xls");
        messageBodyPart.setDataHandler(new DataHandler(source));
        messageBodyPart.setFileName(fileName+".xls");
        multipart.addBodyPart(messageBodyPart);


        message.setContent(multipart);
        return message;
    }
}
