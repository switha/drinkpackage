
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Swith on 20/4/2014.
* generates random actionobjects which are stored in the database
 *  */
public class ActionGenerator {

    /**
     * This will not be distributed with the program and is for testing purposes only!
     * change param in 'new ActionGenerator' to choose the amount of records created
     * edit getDate method to choose the time period of the actions
     * @param args
     */
    public static void main(String[] args) throws SQLException, IOException, ClassNotFoundException {
       new ActionGenerator(2);

       SQLDatabase db = new SQLDatabase();


        db.monthly(5,13);
    }

    private List<ActionObject> actions ;
    private static List<List<ActionObject>> list = new ArrayList<List<ActionObject>>();

    private  static SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    private static SimpleDateFormat sdfYY = new SimpleDateFormat("MMyy");

    /**
     * constructs a new actiongenerator the @param decides how many records will be stored into the database
     * @param n the amonut of records to be created and stored
     */
    public ActionGenerator(int n){

        Database db = new SQLDatabase();
        Random generator = new Random();
        ActionObject model = db.readModel();
        Date date;
        int amt;
        int apples = 0;
        int appleObjects = 0;
       // for (int j = 0; j < 31*365; j++) {

            actions= new ArrayList<>();
            for (int i = 0; i < n; i++) {

                date = getDate();
                model.setName(date + "Simulation");
                model.setDate(date);

                for (Category category : model.getCategories()) {
                    for (Item item : category.getItems()) {

                        amt = generator.nextInt(200)-10;
                        item.setAmountSold(amt>=0?amt:0);

                        if(item.getName().equals("Apple")) {

                            String year = sdfYY.format(date);
                                    if(year.equals("0513")) {
                                        //System.out.println("Apple: " + amt);
                                        apples += amt >= 0 ? amt : 0;
                                        appleObjects++;
                                    }
                        }
                    }
                }
                model.normalize();
                //model.printSelf();
                actions.add(model);

                db.writeAction(model);
                //System.out.println(sdf.format(model.getDate()));
                if(i%100==0)
                    System.out.print(".");
                if(i>=1000&&i%1000==0){
                    System.out.println();
                    System.out.println(i/100+"%");
                }
            }
            list.add(actions);
        System.out.println("Apple total: >"+apples+"<");
        System.out.println("Apple objects: >"+appleObjects+"<");
       // }

    }

    private static Date getDate(){

        int day = 31;
        int month = 4;

        int year = 2013;
        GregorianCalendar calendar = new GregorianCalendar(year,month,day);
        System.out.println(sdf.format(calendar.getTime().getTime()));
        return new Date(calendar.getTime().getTime());
    }


}
