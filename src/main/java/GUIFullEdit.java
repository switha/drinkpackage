import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.*;


/**
 * Created by Switha on 4/5/14.
 * GUI
 * Sets the amount sold of an item
 */

public class GUIFullEdit extends JFrame implements ActionListener {

    private static final long serialVersionUID = 35;


    public static void main(String[] args) {
        DummyDatabase db = new DummyDatabase();
        UtilsV2.setAction(db.readModel());
        new GUIFullEdit();
    }

    private  JPanel outer = new JPanel();


    private JLabel nameL = new JLabel("Name: ");
    private JLabel amountL = new JLabel("Amt: ");
    private JLabel pkgL = new JLabel("Pkg: ");

    private JTextField nameTF = new JTextField("",10);
    private JTextField amountTF = new JTextField("",4);
    private JTextField pkgTF = new JTextField("",4);

    private JButton cancel = new JButton("Cancel");
    private JButton confirm = new JButton("Ok");

    private Font f = UtilsV2.getLargeFont();

   private Item item;




    public GUIFullEdit(Item item){
        super("Editing "+item.getName());

        nameTF.setText(item.getName());
        amountTF.setText(Integer.toString(item.getAmountSold()));
        pkgTF.setText(item.getPackaging());
        this.item = item;
        prepareKeypad();

        createAndShowGUI();
    }


    public GUIFullEdit(){
        super("New");
      createAndShowGUI();
    }

    private void createAndShowGUI(){
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        Container con = this.getContentPane();

        try {
            robby = new Robot();
        } catch (AWTException e) {
            e.printStackTrace();
        }
        prepareKeypad();
        outer.setLayout(new GridLayout(3,3));

        outer.add(nameL);
        outer.add(nameTF);
        outer.add(keypadHolderTop);
        outer.add(pkgL);
        outer.add(pkgTF);
        outer.add(keypadHolderBottom);

        outer.add(amountL);
        outer.add(amountTF);
        outer.add(cancel);




        for (Component component : outer.getComponents()) {
            component.setFont(f);
        }

        confirm.addActionListener(this);
        cancel.addActionListener(this);

        con.add(outer);

        con.setMaximumSize(con.getMaximumSize());


        setVisible(true);

        this.setExtendedState(JFrame.MAXIMIZED_BOTH);


    }

    private void finish(){

        try{
            int amount = Integer.parseInt(amountTF.getText());
            if(amount <0) throw new Exception("Can't have nexgative amount.");


            String name = nameTF.getText();
            if(name.length()<1) throw new Exception("Name has to have some length ...");

            String pkg = pkgTF.getText();
            if(pkg.length()<1) throw new Exception("Packaging has to exist");

            item.setName(name);
            item.setPackaging(pkg);

            item.setAmountSold(amount);


            saveItem(UtilsV2.getAction());
            dispose();
        }
        catch(Exception e){
            e.printStackTrace();
            Toolkit.getDefaultToolkit().beep();
        }

    }

    private void saveItem(ActionObject action){
        OutputStream fos=null;
        BufferedOutputStream bos=null;
        ObjectOutput oos = null;
        try {
            fos = new FileOutputStream("ActionObject.tmp");
            bos = new BufferedOutputStream(fos);
            oos = new ObjectOutputStream(bos);
            oos.writeObject(action);


        } catch (Exception e) {
            e.printStackTrace();
        } finally{

            try{
                oos.close();
                bos.close();
                fos.close();
            } catch (Exception e){e.printStackTrace();}
        }
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        Object source = actionEvent.getSource();
        if(source==cancel){
            dispose();
            return;
        }
        if(source==confirm){
            finish();
            return;
        }
        Component component = getFocusOwner();

        if(component instanceof JTextField) {
            JTextField entry = (JTextField) component;

            for (int i = 0; i < keypad.length; i++) {
                if (source == keypad[i]) {
                    if (i < 10) {
                        d = entry.getDocument();
                        try {
                            d.insertString(d.getLength(),""+i,null);
                        } catch (BadLocationException e) {
                            e.printStackTrace();
                        }
                    }
                    if (i == 10) {
                        //backspace

                        robby.keyPress(KeyEvent.VK_BACK_SPACE);
                    }
                    if (i == 11) {
                        //enter
                        finish();
                    }
                }
            }
        }
    }


    private void prepareKeypad(){
        keypad = new JButton[12];
        keypadHolderTop = new JPanel();
        keypadHolderTop.setLayout(new GridLayout(2,3));
        keypadHolderBottom = new JPanel();
        keypadHolderBottom.setLayout(new GridLayout(2,3));

        for (int i = 0; i < 10; i++) {
            keypad[i] = new JButton(""+i);
            keypad[i].setFocusable(false);
            keypad[i].addActionListener(this);
        }
        for (int i = 1; i < 7; i++) {
            keypadHolderTop.add(keypad[i]);
        }

        for (int i = 7; i < 10; i++) {
            keypadHolderBottom.add(keypad[i]);
        }

        keypad[10] = new JButton("<-");
        keypad[11] = new JButton("ok");
        for (int i = 10; i < 12; i++) {
            keypad[i].setFocusable(false);
            keypad[i].addActionListener(this);
        }
        keypadHolderBottom.add(keypad[10]);
        keypadHolderBottom.add(keypad[0]);

        keypadHolderBottom.add(keypad[11]);
        for (JButton jButton : keypad) {
            jButton.setFont(f);
        }


    }
    private JPanel keypadHolderTop;
    private JPanel keypadHolderBottom;
    private Document d;
    private Robot robby;
    private JButton[] keypad;

}
