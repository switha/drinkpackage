import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Switha on 3/17/14.
 * Represents a category
 * A category contains a list of items
 *
 */
public class Category implements Serializable {

    private static final long serialVersionUID = 45;


    private String name = null;
    private List<Item> items = null;

    /**
     * Creates a new category with only the name specified
     * @param name
     */
    public Category(String name) {
        this.name = name;
    }
    /**
     * Creates a new category with:
     * @param name  Name of the category
     * @param items
     */
    public Category(String name, List<Item> items){
        this(name);
        this.items=items;
        Collections.sort(this.items);
    }

    /**
     * Creates a new category that will be filled out
     */
    public Category(){}



    /**
     *
     * @return the name of the category
     */
    public String getCategoryName() {
        return name;
    }

    /**
     *
     * @return the list of items in the category
     */
    public List<Item> getItems() {
        return items;
    }

    /**
     *
     * @param i the index at which item is to be taken
     * @return the item at index i in the list
     */
    public Item getItem(int i) {return items.get(i);}

    /**
     *
     * @return the number of items that are in the category
     */
    public int getItemAmount(){
        return items.size();
    }

    /**
     * Sets the name of the category
     * @param name
     */
    public void setName(String name){
        this.name=name;
    }

    /**
     * adds param to the list of items
     * can be used even if list is not set
     * @param item is added to the category
     */
    public void addItem(Item item){
        if (items==null) items = new ArrayList<>();
        items.add(item);
    }


    /**
     * sets the amount sold of all items to 0
     */
    public void normalize(){
        for (Item item : items) {
            item.normalize();
        }
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + items.hashCode();
        return result;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Category category = (Category) o;
        if(!name.equals(category.name)) return false;
        if(!items.containsAll(category.items)) return false;

        return true;
    }


    /**
     *
     * @return a list of all the items that have an amount higher than 0
     */
    public List<Item> getFilledItems() {
        List<Item> items = new ArrayList<Item>();
        for (Item item : this.getItems()) {
            if(item.getAmountSold()>0)
                items.add(item);
        }
        return items;
    }


    /**
     * sets the list of items to @param
     * @param items
     */
    public void setItems(List<Item> items) {
        this.items = items;
    }
}
