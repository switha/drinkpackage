import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.*;


/**
 * Created by Switha on 4/5/14.
 * GUI
 * Sets the amount sold of an item
 */

public class GUISetItemAmount extends JFrame implements ActionListener {

    private static final long serialVersionUID = 35;


    private  JPanel outer = new JPanel();
    private JLabel amount = new JLabel("Amount: ");
    private  JLabel packaging=new JLabel();
    private JTextField enter=new JTextField("",4);
    private Item item;
    private JButton cancel = new JButton("Cancel");
    private ActionObject actionObject;
    private Font largeFont = UtilsV2.getLargeFont();
    private Font normalFont = UtilsV2.getFont();
    private Font littleFont = UtilsV2.getLittleFont();

    private Robot robby;
    private JButton[] keypad;
    private JPanel keypadHolder;

    public static void main(String[] args) {
        Database db = new SQLDatabase();
        new GUISetItemAmount(db.readModel().getCategories().get(0).getItem(0),db.readModel());
    }

    /**
     * shows a new window, where the amount of the @param item can be set, and writes the @param actionObject to a temp file
     * @param item the item of which the amount sold will be edited
     * @param actionObject the whole action which is to be saved
     */
    public GUISetItemAmount(Item item, ActionObject actionObject) {
        super(item.getName());
        setBounds(100,100,200,60);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        Container con = this.getContentPane();

        this.actionObject = actionObject;

        prepareKeypad();
        try {
            robby = new Robot();
        } catch (AWTException e) {
            e.printStackTrace();
        }

        this.item=item;
        packaging.setText(item.getPackaging());
        packaging.setFont(largeFont);
        con.add(outer);
        outer.setLayout(new GridLayout(2,4));
        outer.add(amount);
        amount.setFont(largeFont);
        outer.add(enter);
        enter.setFont(largeFont);
        outer.add(packaging);

        outer.add(keypadHolder);


        enter.setFont(largeFont);
        int amount = item.getAmountSold();
        enter.setText((amount < 1) ? "" : Integer.toString(amount));

        JLabel name = new JLabel(item.getName());
        if(item.getName().length()>18)
            name.setFont(littleFont);
        else if(item.getName().length()>9)
            name.setFont(normalFont);
        else name.setFont(largeFont);

        outer.add(name);
        int amt =item.getAmountSold();
        JLabel oldAmoutn = new JLabel(String.valueOf(amt<1?0:amt));
        oldAmoutn.setFont(largeFont);
        outer.add(oldAmoutn);

        outer.add(cancel);
        cancel.setFont(largeFont);
        cancel.addActionListener(this);

       // UtilsV2.showKeyboard();
        pack();



        setVisible(true);
        con.setMaximumSize(con.getMaximumSize());




        this.setExtendedState(JFrame.MAXIMIZED_BOTH);

    }

    private void prepareKeypad(){
        keypad = new JButton[12];
        keypadHolder = new JPanel();
        keypadHolder.setLayout(new GridLayout(4,3));

        for (int i = 0; i < 10; i++) {
            keypad[i] = new JButton(""+i);
            keypad[i].setFocusable(false);
            keypad[i].addActionListener(this);
        }
        for (int i = 1; i < 10; i++) {
            keypadHolder.add(keypad[i]);
        }

        keypad[10] = new JButton("<-");
        keypad[11] = new JButton("Ok");
        for (int i = 10; i < 12; i++) {
            keypad[i].setFocusable(false);
            keypad[i].addActionListener(this);
            keypadHolder.add(keypad[i]);
        }
        keypadHolder.add(keypad[10]);
        keypadHolder.add(keypad[0]);

        keypadHolder.add(keypad[11]);

        for (JButton jButton : keypad) {
            jButton.setFont(largeFont);
        }


    }

    private void finish(){
       try{
           int input = Integer.parseInt(enter.getText());
           if(input <0) throw new Exception();
           item.setAmountSold(input);

           saveItem();
           dispose();
       }
       catch(Exception e){ Toolkit.getDefaultToolkit().beep();}

    }

    private void saveItem(){
        OutputStream fos=null;
        BufferedOutputStream bos=null;
        ObjectOutput oos = null;
        try {
             fos = new FileOutputStream("ActionObject.tmp");
             bos = new BufferedOutputStream(fos);
             oos = new ObjectOutputStream(bos);
            oos.writeObject(actionObject);


        } catch (Exception e) {
            e.printStackTrace();
        } finally{

            try{
                oos.close();
                bos.close();
                fos.close();
            } catch (Exception e){e.printStackTrace();}
        }
    }

    @Override

    public void actionPerformed(ActionEvent actionEvent) {
        Object source = actionEvent.getSource();
        if(source==cancel){
            dispose();
            return;
        }

        Component component = getFocusOwner();

        if(component instanceof JTextField) {
            JTextField entry = (JTextField) component;

            for (int i = 0; i < keypad.length; i++) {
                if (source == keypad[i]) {
                    if (i < 10) {
                       d = entry.getDocument();
                        try {
                            d.insertString(d.getLength(),""+i,null);
                        } catch (BadLocationException e) {
                            e.printStackTrace();
                        }
                    }
                    if (i == 10) {
                        //backspace

                        robby.keyPress(KeyEvent.VK_BACK_SPACE);
                    }
                    if (i == 11) {
                        //enter
                       finish();

                    }
                }
            }
        }
    }

   private Document d;

}
