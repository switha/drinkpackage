import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * Created by Swith on 18/4/2014.
 * GUI
 * allows navigation and selection of items in the supplied category
 */
public class GUIItemSelectWithNavigation extends JFrame implements ActionListener,PropertyChangeListener {

    private static final long serialVersionUID = 35;


    private ActionObject actionObject;
    private Category category;
    private JPanel outer;
    private JPanel navigation;

    private JButton left = new JButton("<");
    private JButton right = new JButton(">");
    private JButton back = new JButton("^");

    private int page = 0;
    private  int pages;
    private java.util.List<Item> items;

    private JButton[] buttons = new JButton[8];

    final private Font f = UtilsV2.getFont();


    /**
     * creates a gui for browsing items of category, creates a 3x3 panel with buttons
     * @param actionObject the action which is worked
     * @param categoryNum the index of the category in the actionObject categories list
     */
    public GUIItemSelectWithNavigation(ActionObject actionObject,int categoryNum) {
        super("Item Select");


        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent we)
            {
                String ObjButtons[] = {"Quit","Cancel"};
                int PromptResult = JOptionPane.showOptionDialog(null,
                        "Are you sure you want to exit?\nYou can open the action again\n when you start DrinkPackage again.",
                        "Are you sure?",
                        JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null,
                        ObjButtons,ObjButtons[1]);
                if(PromptResult==0)
                {
                    System.exit(0);
                }
            }
        });

        this.actionObject = actionObject;
        this.category = actionObject.getCategories().get(categoryNum);
        items =category.getItems();

        left.setEnabled(false);

        navigation=new JPanel();
        navigation.setLayout(new GridLayout(1, 3));
        navigation.add(left);
        left.setFont(f);
        navigation.add(back);
        back.setFont(f);
        navigation.add(right);
        right.setFont(f);
        left.addActionListener(this);
        right.addActionListener(this);
        back.addActionListener(this);


        pages = (items.size() / 8);

        if(items.size()%8==0)
            pages--;
        if(pages==0)

            right.setEnabled(false);


        addPropertyListeners();
        setButtons();


    }

    private void setButtons() {
        outer=new JPanel();


        outer.setLayout(new GridLayout(3,3));
            int start = page * 8;

            for (int i = start; i < start + 8; i++) {

                if (i < items.size()) {
                    int sold = items.get(i).getAmountSold();
                    int amt= ((sold<1)?0:sold);
                    JButton temp = new JButton(items.get(i).getName()+" - "+amt+" ("+items.get(i).getPackaging()+")");
                    buttons[i - start] = temp;
                    temp.addActionListener(this);
                    outer.add(temp);
                    temp.setFont(f);
                } else outer.add(new JLabel());

            }
        outer.add(navigation);
        setContentPane(outer);

        revalidate();
        repaint();

        setVisible(true);
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);



    }



    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        Object source = actionEvent.getSource();
        if(source==back){
            new GUIMainMenu(actionObject);
            dispose();
        }
        if(source==left){
            if(page>0) page--;
            if(page==0)left.setEnabled(false);
            if(page<pages) right.setEnabled(true);
            setButtons();
            return;
        }
        if(source==right){
            if(page<pages) page++;
            if(page==pages) right.setEnabled(false);
            if(page>0) left.setEnabled(true);
            setButtons();
            return;
        }
        for (int i = 0; i < buttons.length; i++) {
            if(source==buttons[i]){


                new GUISetItemAmount(items.get(i+page*8),actionObject);

                return;
            }
        }
    }

    /*
    private void putKbd(){
        Action enterAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent actionEvent){
                new GUIMainMenu(actionObject);
                dispose();
            }
        };

        getRootPane().getInputMap(JComponent.WHEN_FOCUSED).put(KeyStroke.getKeyStroke(
                        KeyEvent.VK_ENTER, 0),
                "check");getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(
                        KeyEvent.VK_ENTER, 0),
                "check");

        getRootPane().getActionMap().put("check",enterAction);

    }
*/

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) { Object source = propertyChangeEvent.getSource();
        setButtons();
    }

    private void addPropertyListeners(){
        for (Item item : items) {
            item.addPropertyChangeListener(this);
        }
    }
}
