/**
 * Created by Switha on 9/29/14.
 */
public class ArchiveObject {

    private static final long serialVersionUID = 36;


    private ActionObject data;
    private int id;


    public ArchiveObject(ActionObject data, int id){
        this.data = data;
        this.id = id;
    }

    public ActionObject getActionObject() {
        return data;
    }

    public int getId() {
        return id;
    }
}
