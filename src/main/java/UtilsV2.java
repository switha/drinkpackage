import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

/**
 * A class with all them utilities and goodies
 *  Created by Switha on 3/17/14.

 */


public  class UtilsV2 {



    /**
     *
     * @param size the amount of items to be stored in the grid
     * @return smallest number of rows and cols
     */

    private static ActionObject action = null;

    public static boolean actionSet(){
        return (action==null);
    }

    public static void setAction(ActionObject inputAction){
        action=inputAction;
    }

    public static ActionObject getAction(){
        return action;
    }

    private static Thread thread = null;
    private static JDialog dialog;

    /**
     * Shows an unclosable promp which says the system is working
     */
    public static void showPrompt(){
      if(dialog!=null) dialog.setVisible(false);
             thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    final JOptionPane optionPane = new JOptionPane("Please wait, system is working.", JOptionPane.INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION, null, new Object[]{}, null);

                    dialog = new JDialog();
                    dialog.setTitle("Please wait.");
                    dialog.setModal(true);

                    dialog.setContentPane(optionPane);

                    dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
                    dialog.pack();
                    dialog.setVisible(true);

                }

            });
            thread.start();

    }

    /**
     * Disables the prompt
     */
    public static void disablePrompt(){
        //thread.interrupt();
        if(dialog!=null) dialog.setVisible(false);
    }

    private static Font font = null;
    private static Font littleFont = null;
    private static Font largeFont = null;

    /**
     * returns the font set in 'font.cfg' else uses default font - Times New Roman, Bold, 30
     * @return a font to be used
     */



    public static Font getLargeFont(){
        if(largeFont==null){
            File f = new File("font.cfg");
            Scanner sc = null;
            if(f.exists()){
                try {
                    sc = new Scanner(new FileReader(f));
                    String family = sc.nextLine();
                    sc.nextLine();
                    sc.nextLine();
                    int size = Integer.parseInt(sc.nextLine());

                    largeFont = new Font(family,Font.BOLD,size);

                } catch (Exception e) {
                    System.out.println("Font file formatted improperly ...");
                    largeFont = new Font("Times New Roman",Font.BOLD,40);

                }
            }
            else{
                largeFont = new Font("Times New Roman",Font.BOLD,40);
            }
        }

        return largeFont;
    }

    public static Font getLittleFont(){
        if(littleFont == null){
            File f = new File("font.cfg");
            Scanner sc = null;
            if(f.exists()){
                try {
                    sc = new Scanner(new FileReader(f));
                    littleFont = new Font(sc.nextLine(), Font.BOLD,Integer.parseInt(sc.nextLine()));
                } catch (FileNotFoundException e) {
                    System.out.println("Font file not formatted properly...");
                    littleFont = new Font("Times New Roman", Font.BOLD,15);
                }
            }else{
                littleFont = new Font("Times New Roman", Font.BOLD,15);
            }
        }
        return littleFont;
    }

    public static Font getFont(){
        if(font==null){
            File f = new File("font.cfg");
            Scanner sc=null;
            if(f.exists()){

                try {
                    sc = new Scanner(new FileReader(f));
                    String family = sc.nextLine();
                    sc.nextLine();
                    int normSize = Integer.parseInt(sc.nextLine());
                    font=new Font(family, Font.BOLD,normSize);
                } catch (FileNotFoundException e) {
                    System.out.println("Font file formatted improperly ...");
                    font = new Font("Times New Roman",Font.BOLD,25);
                }
                if(sc!=null)
                    sc.close();
            }else font = new Font("Times New Roman",Font.BOLD,25);

        }
        return font;
    }



    public static boolean authenticate(String password){
        return(password.equals(getPassword()));
    }

    private static String getPassword(){
        File pwdFile = new File("archive.cfg");
        if(!pwdFile.exists()){
            JOptionPane.showMessageDialog(null,"Password not set, authentication errror","Error",JOptionPane.ERROR_MESSAGE);
            return "sntahoeusrhao9euhrprhur24uchaoheu b3r4h.hurch,0uhaoeuteoknch";
        }

        try {
            Scanner sc = new Scanner(new FileReader(pwdFile));
            String read = sc.nextLine();
            sc.close();
            return read;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        //some unguessable bullcrap
        return "stahoseurchors4hsnahol0euh0'r43haohu098aoe lushoanethu43hc,usaorhu ao0euhanotehu";

    }

}
