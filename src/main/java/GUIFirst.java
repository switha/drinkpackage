import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Swith on 18/4/2014.
 * GUI
 * the start of the program, initilies all the required data
 */
public class GUIFirst extends JFrame{

    private static final long serialVersionUID = 35;


    private ActionObject actionObject;
    private boolean authorsSet;
    private Date date = new Date();
    private JComboBox authors = new JComboBox();
    private Database db;

    private Font f = UtilsV2.getFont();

    /**
     * Starts the program no @param expected or used
     * @param args not used
     */
    public static void main(String[] args) {
        new GUIFirst();
    }
    /**
     * Used for starting up the system - checks whether an unsaved action exist and allows for creation of a new one
     * and if needed initializes the action
     */
    public GUIFirst(){
        readDatabase(); //initializes the database and reads from it
        backup();

        //----Prepare the actionObject--------

        //method unsavedActionExists checks if action exists and if load requestied, loads

        UIManager.put("OptionPane.messageFont", f);
        UIManager.put("OptionPane.inputFont",f);


        if(!UnsavedActionExists()){
            String name = (String) JOptionPane.showInputDialog(null, "Name of Action:", "Name", JOptionPane.QUESTION_MESSAGE);
            if(name==null||name.equals(""))
                System.exit(0);
            //dateSetPrompt();
            actionObject.setName(name);
            actionObject.setDate(date);

            getAuthors();
            if(authorsSet){
                JOptionPane.showMessageDialog(null, authors, "Author:", JOptionPane.QUESTION_MESSAGE);
                actionObject.setAuthor((String)authors.getSelectedItem());}
            else actionObject.setAuthor(JOptionPane.showInputDialog(null,"Author: ","Author",JOptionPane.QUESTION_MESSAGE));
        }

        dispose();
        UtilsV2.setAction(actionObject);
        new GUIMainMenu(actionObject);



    }

    private void readDatabase(){
        db = new SQLDatabase();
        db.makeMonthlyAndYearlyReport();
        actionObject = db.readModel();
    }

    private boolean UnsavedActionExists() {
        File f = new File("ActionObject.tmp");
        if(f.exists()&&JOptionPane.showOptionDialog(null,"Unsent actionObject found. Load?","Incorrect",JOptionPane.OK_OPTION,JOptionPane.WARNING_MESSAGE,null,new String []{"Yes","No"},"Yes")==0){

            try {
                FileInputStream fis = new FileInputStream(f);
                BufferedInputStream bis = new BufferedInputStream(fis);
                ObjectInputStream ois = new ObjectInputStream(bis);
                actionObject =(ActionObject) ois.readObject();
                ois.close();
                bis.close();
                fis.close();
            } catch (Exception e) {
                e.printStackTrace();
                JOptionPane.showOptionDialog(null,"Couldn't read file, default will be loaded.","Problem",JOptionPane.OK_OPTION,JOptionPane.ERROR_MESSAGE,null,new String []{"Ok"},"Ok");
                return false;
            }
            return true;

        }
        return false;

    }

    private SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy");
    private SimpleDateFormat sdfdd = new SimpleDateFormat("dd");


    private void backup(){
    File backup = new File("backup.dat");
        System.out.println("Backing up");

    if(backup.exists()){
        System.out.println("Exists");
        int date;
        try {
            Scanner sc = new Scanner(backup);
            date = Integer.parseInt(sc.nextLine());
            sc.close();
            //if the date is greater than 31 its time to do a monthly backup anyways
            if(date>31) {
                createBackupFile();
                return;
            }

            //otherwise create the backup and incerement the counter by 5
            if(Integer.parseInt(sdfdd.format(new Date()))>= date){
                createBackupFile();
                PrintWriter pw = new PrintWriter(backup);
                pw.print(Integer.toString(Integer.parseInt(sdfdd.format(new Date()))+5));
                pw.close();

            }




        } catch (FileNotFoundException e) {
            createBackupFile();
        }
    } else createBackupFile();

    }
    private void createBackupFile(){
        //System.out.println("Creating new file");
        File backup = new File("backup.dat");
        try {
            PrintWriter pw = new PrintWriter(backup);
            pw.print(sdfdd.format(new Date()));
            pw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        performBackup();

    }
    private void performBackup(){
        //System.out.println("Performing backup");

        File dest = new File("DrinkPackageDatabase_"+sdf.format(new Date())+".BAK");
        File sorce = new File("DrinkPackageDatabase.db");
        try {
            Files.copy(sorce.toPath(),dest.toPath());
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null,"An error occured while:\nCreating the system backup.\nPlease inform your administrator!\n"+e.getMessage()+"\n" +
                    "You may continue using the system normally.","Error",JOptionPane.WARNING_MESSAGE);
        }

    }

    private void getAuthors(){
        File f = new File("authors.cfg");
        if(!f.exists()){
            authorsSet=false;
            return;
        }
        List<String> a = new ArrayList<>();
        Scanner sc = null;
        try {
             sc = new Scanner(new FileReader(f));
            while(sc.hasNextLine())
                a.add(sc.nextLine());
            authors=new JComboBox(a.toArray());
        } catch (FileNotFoundException e) {
            authorsSet=false;
            return;

        }finally{
            if(sc != null)
                sc.close();
        }
        authors.setFont(this.f);
        authorsSet=true;
    }

}
