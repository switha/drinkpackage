import com.itextpdf.text.DocumentException;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.write.*;
import jxl.write.Number;

import java.io.File;
import java.io.IOException;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Switha on 4/12/14.
 * creates an excel file from the supplied actionobject
 */
public class CreateExcel {

    private static String top = "Fill this table carefully and hand in with the Daily Report";

    private static WritableSheet sheet;


    //CHANGE THIS IF YOU WANT TO HAVE TWO ROWS
    private static final int MAXROWS = 999;

    private File file;

    /**
     *
     * @param items An array of lists, where the array is either 12 or 31 long depending on whether @monthly. Each lists contains all the items sold
     * @param other true if the excel is to be created for Category Other
     * @param monthly true if its a monthly report
     * @param month the month for which the report is to be created
     * @param year they ear for which the month is to be created, only the last two digits
     * @throws IOException
     * @throws WriteException
     * @throws DocumentException
     */
    public CreateExcel(List<Item>[] items, boolean other, boolean monthly, int month, int year)throws IOException, WriteException, DocumentException {


        year+=2000;
        String fileName;

        if(other){
            if(monthly) fileName = "Monthly_Other_"+month+"_"+year;
            else fileName = "Yearly_Other_"+year;
        }
        else{
            if(monthly) fileName = "Monthly_"+month+"_"+year;
            else fileName = "Yearly_"+year;
        }
        System.out.println("CREATING EXCEL");
        createMonthly(items,fileName,monthly,month,year);


    }

    private void createMonthly(List<Item>[] items,String fileName,boolean monthly,int month, int year) throws IOException, WriteException {
        file = new File(fileName+".xls");
        WritableWorkbook workbook = Workbook.createWorkbook(file);
        WritableSheet sheet = workbook.createSheet(monthly?"Monthly":"Yearly", 0);
        WritableFont boldFont = new WritableFont(WritableFont.TIMES,12,WritableFont.BOLD);
        WritableFont normalFont = new WritableFont(WritableFont.TIMES,12);
        WritableCellFormat b = new WritableCellFormat(boldFont);
        WritableCellFormat n = new WritableCellFormat(normalFont);
        b.setAlignment(Alignment.LEFT);
        n.setAlignment(Alignment.LEFT);

        sheet.addCell(new Label(0,0,monthly?(getMonth(month)+" "+year):Integer.toString(year),n));
        sheet.addCell(new Label(0,1,"Name:",n));
        Item[][] preparedItems = prepareItems(items);
        System.out.println("PRINTING PREPARED ITEMS");
        for (Item[] preparedItem : preparedItems) {
            for (Item item : preparedItem) {

            }
        }
        System.out.println("END PRINTING PREPARED ITEMS");


        //print the 1 - total
        for (int col = 2; col < preparedItems.length+2; col++) {
            if (col < preparedItems.length+1) sheet.addCell(new Number(col, 1, col-1,b));
            else sheet.addCell(new Label(col, 1, "Total:",b));

        }
        //print name and packaging
            for (int row = 2; row < preparedItems[0].length+1; row++) {
                    sheet.addCell(new Label(0, row, preparedItems[preparedItems.length - 1][row - 2].getName(), b));
                    sheet.addCell(new Label(1, row, preparedItems[preparedItems.length - 1][row - 2].getPackaging(), b));


            }

        System.out.println("Printing values");
        //print the values
        System.out.println("There are "+preparedItems[0].length+1+" rows");
        for (int col = 2; col < preparedItems.length+2; col++) {
            for (int row = 2; row < preparedItems[0].length+1; row++) {

                sheet.addCell(new Number(col,row,preparedItems[col-2][row-2].getAmountSold()));
            }
        }

        workbook.write();
        workbook.close();

        new AutoSizeExcel(file);

    }

    //makes sure all the lists are consistent and adds a  total list
    private Item[][] prepareItems(List<Item>[] input){
        List<Item> total = new ArrayList<>();


        for (List<Item> itemList : input) {
            for (Item item : itemList) {
                if (total.contains(item)) continue;
                //add a blank into total
                total.add(new Item(item.getName(),item.getPackaging()));


            }
        }

        int innerSize = total.size();


        Item[] totalArray = listToArray(total);




        //create an array of item arrays for every month/year + the final list, and item array contains all items that occur
        Item[][] finalList = new Item[input.length+1][innerSize];

       int apples = 0;

        for (int outerIndex = 0; outerIndex < input.length; outerIndex++) {
             Item[] inner = new Item[innerSize];
            for (int i = 0; i < innerSize; i++) {
                for (Item item : input[outerIndex]) {
                    if(totalArray[i].equals(item)) {
                        inner[i] = item;
                        totalArray[i].addAmount(item.getAmountSold());
                        if(item.getName().equals("Apple")) {
                            //System.out.printf("Apple in %d is %d\n", outerIndex, item.getAmountSold());
                            apples+=item.getAmountSold();
                        }
                    }
                }
            }
            finalList[outerIndex] = inner;
        }

        finalList[finalList.length-1] = totalArray;




        for (int i = 0; i < finalList.length - 1; i++) {
            Item[] inner = finalList[i];
            for (int j = 0; j < inner.length; j++) {
                if(inner[j]==null)
                    inner[j] = new Item();
            }
        }

        /*
        for (Item item : totalArray) {
            if(item.getName().equals("Apple")){
                System.out.println("Calculated apple :"+item.getAmountSold());
                System.out.println("Correct apple :"+apples);
            }
        }
        */
        /*
        System.out.println("Ronud 2:");
        for (Item[] items : finalList) {
            for (Item item : items) {
                if (item.getName().equals("Apple")) {
                    System.out.println("Apple :"+item.getAmountSold());
                }
            }

        }

*/
        return finalList;

    }

    private Item[] listToArray(List<Item> input){
        Item[] output = new Item[input.size()];
        for (int i = 0; i < output.length; i++) {
            output[i] = input.get(i);
        }
        return output;
    }

    private String getMonth(int month){

        System.out.println("Its for the month: "+month);
        return new DateFormatSymbols().getMonths()[month-1];
 }
    /**
     * creates an excel from the supplied actionObject
     * @param a the action object
     * @throws IOException
     * @throws WriteException
     * @throws DocumentException
     */
    public CreateExcel(ActionObject a) throws IOException, WriteException, DocumentException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        file=new File(a.getFileName()+".xls");
        WritableWorkbook workbook = Workbook.createWorkbook(file);
        WritableSheet sheet = workbook.createSheet("Daily", 0);

        WritableFont boldFont = new WritableFont(WritableFont.TIMES,12,WritableFont.BOLD);
        WritableFont normalFont = new WritableFont(WritableFont.TIMES,12);

        //merge the first row
        sheet.mergeCells(0,0,5,0);

        WritableCellFormat b = new WritableCellFormat(boldFont);
        WritableCellFormat n = new WritableCellFormat(normalFont);
        b.setAlignment(Alignment.LEFT);
        n.setAlignment(Alignment.LEFT);
        b.setBorder(Border.ALL, BorderLineStyle.THIN);
        n.setBorder(Border.ALL, BorderLineStyle.THIN);

        Label label = new  Label(0,0,top,b);
        sheet.addCell(label);



        sheet.addCell( new Label(0,1,"Date"));

        sheet.addCell( new Label(1,1,sdf.format(a.getDate())));
        sheet.addCell(new Label(2,1,"Group Name:"));
        sheet.addCell(new Label(3,1,a.getName()));
        sheet.addCell(new Label(4,1,"Written by:"));
        sheet.addCell(new Label(5,1,a.getAuthor()));

        sheet.addCell(new Label(0,2,"Amt",b));

        sheet.addCell(new Label(1, 2, "Name", b));
        sheet.addCell(new Label(2, 2, "Pkg", b));
        if(MAXROWS<100) {
            sheet.addCell(new Label(3, 2, "Amt", b));
            sheet.addCell(new Label(4, 2, "Name", b));
            sheet.addCell(new Label(5, 2, "Pkg", b));
        }

        int col = 0;
        int row =3;

        a.normalize();
        List<Category> categories = a.getCategories();
        for (int i = 0; i <categories.size(); i++) {
            Category cat = categories.get(i);
            List<Item> items = cat.getItems();

            if(MAXROWS-items.size()-row<1){ col+=3;row=3;}
            boolean other = false;
            if(cat.getCategoryName().equals("Other"))
                other = true;
            sheet.addCell(new Label(1 + col, row, cat.getCategoryName(), b));
            for (int j = 0; j < items.size(); j++) {
                if(other){

                    Item item = items.get(j);
                    int amount = item.getAmountSold();
                    if(amount>0){
                        row ++;
                        sheet.addCell(new Number(0 + col, row, amount,n));
                        sheet.addCell(new Label(1 + col, row, item.getName(), n));
                        sheet.addCell(new Label(2 + col, row, item.getPackaging(), n));
                    }

                }
                else{
                    row++;
                    Item item = items.get(j);
                    int amount = item.getAmountSold();
                    sheet.addCell(new Number(0 + col, row, amount,n));
                    sheet.addCell(new Label(1 + col, row, item.getName(), n));
                    sheet.addCell(new Label(2 + col, row, item.getPackaging(), n));
                }



            }
            row++;//addItem a blank


        }


        workbook.write();
        workbook.close();

        new AutoSizeExcel(file);



    }


}
