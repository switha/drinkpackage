import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Switha on 3/17/14.
 * Contains a predefined model action
 * It is a dummy, however it is used to create the 'first' database
 */
public class DummyDatabase implements Database {

    ArrayList<Category> c;

    /**
     * SQL database initializator, contains predefined categories
     */
    public DummyDatabase(){
        generateCategories();
    }

    private void generateCategories() {
        c= new ArrayList<Category>();
                c.add(water());
                c.add(juice());
                c.add(cola());
                c.add(wine());
                c.add(tea());
                c.add(coffee());
                c.add(beer());


    }

    private Category coffee(){
            ArrayList<Item> c =new ArrayList<Item>();
                    c.add(new Item("Decaf","pcs"));
                    c.add(new Item("Breakfast","pcs"));
                    c.add(new Item("Hot chocolate","pcs"));
                    c.add(new Item("Espresso","pcs"));
                    c.add(new Item("Cappuccino","pcs"));
                    c.add(new Item("Latte","pcs"));
            
        return new Category("Coffee",c);
    }

    private Category wine(){
        String pre = "House wine";
        Item white = new Item(pre+"White","0,75");
        Item red = new Item(pre+"Red","0,75");
        Item whiteSmall =new Item(pre+"White","0,15");
        Item redSmall = new Item(pre+"Red","0,15");
        ArrayList<Item> list = new ArrayList<Item>();
        list.add(white);
        list.add(red);
        list.add(whiteSmall);
        list.add(redSmall);
        Category wine = new Category("Wine", list);
        return wine;
    }

    private Category water(){
    ArrayList<Item> c = new ArrayList<Item>();

                            c.add(new Item("Matonni Grand","0,75l"));
                            c.add(new Item("Aquilla Grand","0,75l"));
                            c.add(new Item("Sparkling Water","1.5l"));
                            c.add(new Item("Still Water","1,5l"));
                            c.add(new Item("Bonaqua sparkling","0,25l"));
                            c.add(new Item("Bonaqua semi sparkling","0,25l"));
                            c.add(new Item("Bonaqua still","0,25l"));
        return new Category("Waters", c);

    }

    private Category juice(){
        String p = "0,2l";
        ArrayList<Item> c = new ArrayList<Item>();

                        c.add(new Item("Apple",p));
                        c.add(new Item("Multivitamin",p));
                        c.add(new Item("Pomegrante",p));
                        c.add(new Item("Grapefruit",p));
                        c.add(new Item("Strawberry",p));
                        c.add(new Item("Pear",p));
                        c.add(new Item("Blackcurrant",p));
        return new Category("Juice",c);
    }

    private Category cola(){
        String p1="0,33l";
        String p2="0,2l";
        ArrayList<Item> c = new ArrayList<Item>();
                        c.add(new Item("Coca Cola",p1));
                        c.add(new Item("Coca Cola Zero", p1));
                        c.add(new Item("Coca Cola light",p2));
                        c.add(new Item("Kinley tonic",p2));
                        c.add(new Item("Kinley bitter lemon",p2));
                        c.add(new Item("Kinley ginger ale",p2));
                        c.add(new Item("Fanta",p2));
                        c.add(new Item("Sprite",p1));
        return new Category("Coca cola Products",c);


    }

    private Category tea(){
        String p="pcs";
                ArrayList<Item> c = new ArrayList<Item>();
                        c.add(new Item("Darjeeling",p));
                        c.add(new Item("English Breakfast",p));
                        c.add(new Item("Earl Grey",p));
                        c.add(new Item("Japan Classic",p));
                        c.add(new Item("Rooibos Vanilla",p));
                        c.add(new Item("Peppermint",p));
                        c.add(new Item("Chamomile",p));
                        c.add(new Item("Red Berries",p));
                        c.add(new Item("Lemon Sky",p));
                        c.add(new Item("Jasmin",p));
                        c.add(new Item("WinterDream",p));
                        c.add(new Item("Black to Relax",p));
        return new Category("Tea/Restaurant",c);

    }

    private Category beer(){
        String p1,p2,p3;
        p1="0,33l";
        p2="0,5l";
        p3="0,3l";

               ArrayList<Item> c = new ArrayList<Item>();
                        c.add(new Item("Budejovicky Budvar", p1));
                        c.add(new Item("Staropramen Dark",p2));
                        c.add(new Item("Staropramen Lezak",p1));
                        c.add(new Item("Stella Artois",p1));
                        c.add(new Item("Stella Artois Non Alcoholic",p1));
                        c.add(new Item("Pilsner Urquel",p1));
                        c.add(new Item("Staropramen Lezak-tocene",p3));
                        c.add(new Item("Staropramen granat-tocene",p3));

        return new Category("Beer",c);


    }

    @Override
    public ActionObject readModel() {
        return new ActionObject(new Date(), "Model",c);
    }

    @Override
    public void makeMonthlyAndYearlyReport() {

    }


    @Override
    public void writeAction(ActionObject actionObject) {

    }
}
