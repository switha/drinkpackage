import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Created by Switha on 3/30/14.
 * Represents an action
 * an actionObject contains a list of categories
 * This object is stored in the SQL database
 */
public class ActionObject implements Serializable{

    private static final long serialVersionUID = 55;


    private String author;
    private  Date date;
    private  String name;
    private List<Category> categories;

    /**
     *
     * @param date the date of the action
     * @param name the name of the action
     * @param categories a list of categories
     */
    public ActionObject(Date date, String name, List<Category> categories){
        this(date,name);
        this.setCategories(categories);
    }

    /**
     *
     * @param date the date of the action
     * @param name the name of the action
     */
    public ActionObject(Date date, String name){
        this.setDate(date);
        this.setName(name);
    }

    /**
     *
     * @return the date of the action
     */
    public Date getDate() {
        return date;
    }

    /**
     *
     * @return the name of the action
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @return a list of the categories in the action
     */
    public List<Category> getCategories() {
        return categories;
    }

    /**
     * sets/changes the list of categories of the action to @param
     * @param categories the list to be changed to
     */
    public void setCategories(List<Category> categories){
        this.categories=categories;
    }

    /**
     *  sets the date of the action to @param
     * @param date the date to be set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * sets the name of the action to @param
     * @param name the name to be set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * sets the amonut of all the items in all the categories to 0
     */
    public void normalize(){
        for (Category category : categories) {
            category.normalize();
            if(category.getCategoryName().equals("Other")){
                Category temp = category;
                categories.remove(temp);
                categories.add(temp);
            }
        }
    }

    /**
     *
     * @return the name of the file tha action is to be saved to excluding the extension; generadted from name and date
     */
    public String getFileName(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(getDate())+"_"+getName();
    }

    /**
     * gets the author of the action
     * @return the author that is to be set
     */
    public String getAuthor() {
        return author;
    }

    /**
     * sets the author of the action
     * @param author the author to be set
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * adds a category to the list of categories
     * @param c the category to be added
     */
    public void addCategory(Category c){
        categories.add(c);
    }

    /**
     * returns a list of all the items that have a value >0
     * @return ^
     */
    public List<Item> getFilledItems(){
       List<Item> items = new ArrayList<Item>();
        for (Category category : categories) {
            List<Item> categoryItems = category.getFilledItems();
            for (Item categoryItem : categoryItems) {
                items.add(categoryItem);
            }
        }
        return items;
    }

    /**
     * used for creating models, takes all the current categories, sets the amount sold of all the items to 0,
     * and checks whether @param contains any other categories, which are otherwise added
     * @param list the list to be checked with
     */
    public void checkCategories(List<Category> list){
        for (Category category : list) {
            category.normalize();
            if(!categories.contains(category)){
                addCategory(category);
            } else{
                    //TODO WHAT?
            }
        }

    }

    /**
     * removes the category at @Param
     * @param index
     */
    public void removeCategory(int index){
        if(index>=categories.size()){
            System.out.printf("Trying to remove %d, size is only %d\n",index,categories.size());
            return;
        }
        categories.remove(index);

    }

    /**
     * prints the contents of the action onto the terminal
     * this is for debugging purposes
     */
    public void printSelf(){
        System.out.println("----START ACTION----");
        System.out.print("ActionObject: " + getName());
        if(getDate() !=null) System.out.println("   Date: "+ getDate().toString());
        else System.out.println();
        for (Category category : getCategories()) {
            System.out.println("\n>"+category.getCategoryName());
            for (Item item : category.getItems()) {
                System.out.println(">>"+item.getName()+": "+item.getAmountSold()+" ["+item.getPackaging()+"]");
            }
        }
        System.out.println("----END ACTION----");
    }
}
